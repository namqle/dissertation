close all;
clear variables;

%% Get the jobset directories

img_dir = pwd;
data_dir = '/Applications/Computation/nqlmd/research/1d_nemd_dos/';

cd(data_dir);

%% Plot settings

X = 3.2;
Y = 2.5;

% Axes
set(0, 'DefaultAxesBox', 'on');
set(0, 'DefaultAxesLineWidth', 0.5);
set(0, 'DefaultAxesXMinorTick', 'on');
set(0, 'DefaultAxesYMinorTick', 'on');
set(0, 'DefaultAxesTickDir', 'in');

% Plots
set(0, 'DefaultLineLineWidth', 1);
set(0, 'DefaultLineMarkerSize', 12);

% Figures
set(0, 'DefaultFigurePaperSize', [X Y]);
set(0, 'DefaultFigurePaperPositionMode', 'manual');
set(0, 'DefaultFigurePaperPosition', [ 0 0 X Y ] );
set(0, 'DefaultFigurePosition', [ 0 0 X*72 Y*72 ] );

% Comment here to switch on/off Times font
set(0, 'DefaultTextFontname', 'Times New Roman', ...
       'DefaultTextFontsize', 12, ...
       'DefaultAxesFontname', 'Times New Roman', ...
       'DefaultAxesFontsize', 12 );

%% Constants

kB = 1.38065e-23; % J/K
amu = 1.660468e-27; % kg
K = 2.64327; % N/m

a = 5.313e-10 / 2;

%% Read data

raw = load('jobset03-r00-harmonic_all/T10/1d.micro');

Natoms = max(raw(:,1)+1);
Nframes = size(raw,1) / Natoms;

raw = reshape(raw, Natoms, Nframes, size(raw,2));

v = raw(:, :, 8);
v = (1e12)*(1e-10) * v;

Natoms = size(v,1);
Nbin = 6*6*4;

dt = 0.002e-12 * 50;

%% Plot velocity distribution

figure(1); clf; hold on;

bin_width = 5;
vv = (-200 : bin_width : 200)';

T = zeros(2,1);

% Once on left, once on right
for material = 1 : 2
  
  %% Settings for local velocity distribution
  if material == 1
    start_index = 20 * Nbin;
    color = 'r';
    m = 40 * amu;
  else
    start_index = 50 * Nbin;
    color = 'b';
    m = 120 * amu;
  end
  
  atom_index = start_index:start_index+10*Nbin;
  tally = zeros(size(vv,1), size(v,2));
  for frame = 1 : size(v,2)
    [tally(:,frame), ~] = hist(v(atom_index,frame), vv);
  end
  
  %% Measured f(v)
  
  measured_distro = mean(tally,2) / (length(atom_index)*bin_width);
  measured_distro_std = std(tally,0,2) / (length(atom_index)*bin_width * sqrt(size(v,2)));
  
  plot(vv, measured_distro, '.', 'color', color);
  
  %% Predicted f(v) (Maxwell-Boltzmann)
  
  T(material) = mean( v(start_index: start_index+10*Nbin, end).^2 ) * m / kB;
  MB_distro = sqrt(m/(2*pi*kB*T(material))) * exp(-m*vv.^2 / (2*kB*T(material)));
  
  plot(vv, MB_distro, '-', 'color', color);

end

% Labels and stuff

legend('Left', ['M-B (' num2str(T(1), '%.1f') ' K)'], 'Right', ['M-B (' num2str(T(2), '%.1f') ' K)'], 'location', 'nw');
legend boxoff;
xlabel('{\it{v}}, m/s')
ylabel('{\it{g(v)}}, (m/s)^{-1}');
axis([-100 100 0 0.04]);

text(70, 0.035, '(a)');

print(gcf, '-dpdf', sprintf('%s/validation_T_harmonic_speeds.pdf', img_dir));

%% Density of states calculations

figure(2); clf; hold on;
f_scale = 1e-12;

N_samples = size(v,2);
freq = 1 / dt * linspace(0, 1, N_samples);
dfreq = freq(2) - freq(1);

domain_size = Natoms*a;

% Once on left, once on right
for material = 1 : 2
  
  %% Settings for local DOS
  if material == 1
    start_index = 20 * Nbin;
    color = 'r';
    m = 40 * amu;
  elseif material == 2
    start_index = 50 * Nbin;
    color = 'b';
    m = 120 * amu;
  end
  atom_index = start_index : start_index+10*Nbin; % DOS window
  
  T_local = T(material);
  
  %% Predicted DOS
  
  pdos_ideal = @(freq) 2*pi * 2/(pi*a) * real((4*K/m - (2*pi*freq).^2).^(-0.5));
  
  plot(f_scale*freq, pdos_ideal(freq), '-', 'color', 0.5*[1,1,1]);
  
  %% Measured DOS (Wiener--Khinchin)
  
  fft_v = fft(v(atom_index,:), N_samples, 2) / N_samples;
  
  pdos_wk = abs(fft_v).^2;
  pdos_wk = m * mean(pdos_wk, 1);
  pdos_wk = pdos_wk * Natoms /(kB) / (dfreq * domain_size);
  
  T_DOS = trapz(freq, pdos_wk) * a;
  pdos_wk = pdos_wk / T_DOS;
  
  plot(f_scale*freq(1:N_samples/2+1), 2*pdos_wk(1:N_samples/2+1), '-', 'color', color);
  
  if material == 1
    text(2.1, 0.0145, '40 amu,');
    annotation('textarrow', [0.78, 0.7], [0.6, 0.4], 'string', sprintf('%.1f K', T_DOS), 'HeadLength', 7, 'HeadWidth', 7);
  elseif material == 2
    text(0.15, 0.0105, '120 amu,');
    annotation('textarrow', [0.355, 0.4], [0.45, 0.32], 'string', sprintf('%.1f K', T_DOS), 'HeadLength', 7, 'HeadWidth', 7);
  end
  
end

% %% Labels and stuff

xlabel('{\it{f}},  THz');
ylabel('{\it{D}}( {\it{f}} ),  Hz^{-1} m^{-1}');
axis([0, 3, 0, 0.02]);

annotation('textarrow', [0.45, 0.47], [0.82, 0.71], 'string', 'Theory (harmonic)', 'HeadLength', 7, 'HeadWidth', 7);
annotation('arrow', [0.55, 0.65], [0.82, 0.78], 'HeadLength', 7, 'HeadWidth', 7);

text(2.55, 0.0175, '(b)');

print(gcf, '-dpdf', sprintf('%s/validation_T_harmonic_DOS.pdf', img_dir));