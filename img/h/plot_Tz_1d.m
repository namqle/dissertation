clear variables;
close all;

%% Get the jobset directories

img_dir = pwd;
data_dir = '/Applications/Computation/nqlmd/research/1d_nemd/';

T = 6;

cd(data_dir);

[~, dirs] = system('ls -d1 jobset{28,29,30}-r01-*');
dirs = textscan(dirs, '%s');
dirs = dirs{:};

%% Manual plot settings
X = 2.1;
Y = 1.3;

% Axes
set(0, 'DefaultAxesBox', 'on');
set(0, 'DefaultAxesLineWidth', 0.5);
set(0, 'DefaultAxesXMinorTick', 'on');
set(0, 'DefaultAxesYMinorTick', 'on');
set(0, 'DefaultAxesTickDir', 'in');

% Plots
set(0, 'DefaultLineLineWidth', 0.5);
set(0, 'DefaultLineMarkerSize', 5);

% Figures
set(0, 'DefaultFigurePaperSize', [X Y]);
set(0, 'DefaultFigurePaperPositionMode', 'manual');
set(0, 'DefaultFigurePaperPosition', [ 0 0 X Y ] );
set(0, 'DefaultFigurePosition', [ 0 0 X*72 Y*72 ] );

% Comment here to switch on/off Times font
set(0, 'DefaultTextFontname', 'Times New Roman', ...
       'DefaultTextFontsize', 12, ...
       'DefaultAxesFontname', 'Times New Roman', ...
       'DefaultAxesFontsize', 12 );

     
%% Specify simulation directory(ies) to plot

% For each directory...
for dir_num = 1 : length(dirs)
  
  job_dir = dirs{dir_num};
  job_type = job_dir(14:end);
  
  cd(sprintf('%s/%s/T%02d/', data_dir, job_dir, T));
  
  %% Load the temperature profile data
  profile_data = load('1d.profile');
  
  Nbins = max(profile_data(:,1)) + 1;
  profile_data = reshape(profile_data, Nbins, [], 3);
  
  a = 5.313; % angstroms
  a = 72*a;  % each bin in the 1D sim is 72 "unit cells"
  bin = profile_data(:, :, 1);
  T_mean = profile_data(:, :, 2);
  T_std = profile_data(:, :, 3);
  
  bin = bin(:, 1);
  T_profile = mean(T_mean, 2);
  T_profile_std = std(T_mean, 0, 2);

  %% Fit linear profiles to the "bulk-like" regions
  
  zlo_L = 20;
  zhi_L = 35;
  T_L = T_profile(zlo_L*2 : zhi_L*2);
  z_L = a/2 * (zlo_L*2 : zhi_L*2)';
  p_L = polyfit(z_L, T_L, 1);
  
  zlo_R = 45;
  zhi_R = 60;
  T_R = T_profile(zlo_R*2 : zhi_R*2);
  z_R = a/2 * (zlo_R*2 : zhi_R*2)';
  p_R = polyfit(z_R, T_R, 1);
  
  %% Make the plots
  
  figure(dir_num);
  
  % The temperature profile
  hold on;
  bin_list = (1:Nbins) / 2 + 0.25;
  shadedErrorBar((bin_list(3:end-2)-40)*a/10000, T_profile(3:end-2), T_profile_std(3:end-2), {'k.', 'linewidth', 0.5});
  
  % The fitted regions
  if strcmp(job_type, 'lj')
    color = 'b';
  elseif strcmp(job_type, 'harmonic')
    color = 'r';
  elseif strcmp(job_type, 'lj_0uc')
    color = 'g';
  else
    error('don''t recognize the job_type');
  end
  
  plot((z_L - 40*a)/10000, polyval(p_L, z_L), '-', 'color', color, 'linewidth', 2);
  plot((z_R - 40*a)/10000, polyval(p_R, z_R), '-', 'color', color, 'linewidth', 2);
  
  hold off;
  
  % Labels, etc
  T_overall = mean(T_profile(3:end-2));
  T_L = mean(T_profile(zlo_L*2 : zhi_L*2));
  T_R = mean(T_profile(zlo_R*2 : zhi_R*2));
  xlabel('{\it{z}},  {\mu}m');
  ylabel('{\it{T}},  K');
  
  axis([-1.5, 1.5, 4, 8]);
  set(gca, 'XTick', -1.5 : 0.75 : 1.5);
  set(gca, 'YTick', 4 : 2 : 12);
  set(gca, 'TickLength', [0.02, 0.25]);
  
  text(0.9, 7, sprintf('(%s)', char(96+dir_num)));
  
  %% Save stuff
  
  print(gcf, '-dpdf', sprintf('%s/Tz_%s_1d.pdf', img_dir, job_type));
  
  %% Go back up
  cd ..
end
