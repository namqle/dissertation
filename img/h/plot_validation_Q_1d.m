clear variables;
close all;

%% Get the jobset directories

img_dir = pwd;
data_dir = '/Applications/Computation/nqlmd/research/1d_nemd/';

cd(data_dir);

% [~, more_dirs] = system('ls -d1 jobset29-r*');
% more_dirs = textscan(more_dirs, '%s');
% more_dirs = more_dirs{:};
% 
% [~, even_more_dirs] = system('ls -d1 jobset3[34]-r*');
% even_more_dirs = textscan(even_more_dirs, '%s');
% even_more_dirs = even_more_dirs{:};
% 
% dirs = [dirs; more_dirs; even_more_dirs];

%% Manual plot settings
X = 3.2;
Y = 2.5;

% Axes
set(0, 'DefaultAxesBox', 'on');
set(0, 'DefaultAxesLineWidth', 0.5);
set(0, 'DefaultAxesXMinorTick', 'on');
set(0, 'DefaultAxesYMinorTick', 'on');
set(0, 'DefaultAxesTickDir', 'in');

% Plots
set(0, 'DefaultLineLineWidth', 1.0);
set(0, 'DefaultLineMarkerSize', 12);

% Figures
set(0, 'DefaultFigurePaperSize', [X Y]);
set(0, 'DefaultFigurePaperPositionMode', 'manual');
set(0, 'DefaultFigurePaperPosition', [ 0 0 X Y ] );
set(0, 'DefaultFigurePosition', [ 0 0 X*72 Y*72 ] );

% Comment here to switch on/off Times font
set(0, 'DefaultTextFontname', 'Times New Roman', ...
       'DefaultTextFontsize', 12, ...
       'DefaultAxesFontname', 'Times New Roman', ...
       'DefaultAxesFontsize', 12 );

figure(1);
hold on;

%% Read fitted data from each run and keep running averages

data = zeros([12 2 6]);

for run = 0 : 5
  
  cd(['jobset05-r0' num2str(run) '-lj_vary_flux']);
  
  fid = fopen('hBD.dat', 'r');
  raw = textscan(fid, '%f %f %f %f %f %f %f', 'CommentStyle', '#');
  fclose(fid);
  
  data(:,:,run+1) = [raw{1} raw{7}];

  cd ..
  
end

data_ave = mean( data, 3 );
data_std = 2*std( data, 0, 3 )/sqrt(size(data,3));

%% Plot

h_scale = 1e12;
Q_scale = 1e12;

T05_mask = 1 : 7;
T08_mask = 8 : 12;

hold on;
Q_T05 = (3 : 2 : 15) * 2e-12;
Q_T08 = (8 : 2 : 16) * 2e-12;
errorbar(Q_scale*Q_T05, h_scale*data_ave(T05_mask,2), h_scale*data_std(T05_mask,2), 'b.');
errorbar(Q_scale*Q_T08, h_scale*data_ave(T08_mask,2), h_scale*data_std(T08_mask,2), 'r.');

axis([0, 40, 0, h_scale*1.50001e-10]);
xlabel('Heat current,  pW');
ylabel('{\it{h}},  pW K^{-1}');

%% Annotate

annotation('textarrow', [0.465, 0.445], [0.6, 0.32], 'string', 'Final current used', 'HeadLength', 7, 'HeadWidth', 7);
annotation('arrow', [0.55, 0.60], [0.6, 0.44], 'HeadLength', 7, 'HeadWidth', 7);

text(28, 15, '{\it{T}} = 5 K');
text(29, 70, '{\it{T}} = 8 K');

%% Save

cd(img_dir);

print(gcf, '-dpdf', 'validation_Q_1d.pdf');