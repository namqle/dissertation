clear variables;
close all;

%% Get the jobset directories

img_dir = pwd;
data_dir = '/Applications/Computation/nqlmd/research/3d_nemd/';

cd(data_dir);

[~, dirs] = system('ls -d1 jobset{30,31,32}-r0*');
dirs = textscan(dirs, '%s');
dirs = dirs{:};

% [~, more_dirs] = system('ls -d1 jobset11-r0[0-5]-*');
% more_dirs = textscan(more_dirs, '%s');
% more_dirs = more_dirs{:};
% 
% dirs = [dirs; more_dirs];

%% Manual plot settings
X = 6.5;
Y = 3;

% Axes
set(0, 'DefaultAxesBox', 'on');
set(0, 'DefaultAxesLineWidth', 0.5);
set(0, 'DefaultAxesXMinorTick', 'on');
set(0, 'DefaultAxesYMinorTick', 'on');
set(0, 'DefaultAxesTickDir', 'in');

% Plots
set(0, 'DefaultLineLineWidth', 1.0);
set(0, 'DefaultLineMarkerSize', 12);

% Figures
set(0, 'DefaultFigurePaperSize', [X Y]);
set(0, 'DefaultFigurePaperPositionMode', 'manual');
set(0, 'DefaultFigurePaperPosition', [ 0 0 X Y ] );
set(0, 'DefaultFigurePosition', [ 0 0 X*72 Y*72 ] );

% Comment here to switch on/off Times font
set(0, 'DefaultTextFontname', 'Times New Roman', ...
       'DefaultTextFontsize', 12, ...
       'DefaultAxesFontname', 'Times New Roman', ...
       'DefaultAxesFontsize', 12 );

figure(1);
hold on;

%% Go into each jobset and add to the plot

job_types = {};
for dir_num =  1 : length(dirs)
  
  % Move into the jobset directory
  job_dir = dirs{dir_num};
  cd(job_dir);
  
  % Determine what kind of simulation it was
  %   (from the directory name)
  temp = find(job_dir == '-', 1, 'last');
  job_type = job_dir(temp+1:end);
  
  job_num = str2double( job_dir(temp-2 : temp-1) ) + 1;
  
  clear temp;
  
  % Load the data
  fid = fopen('hBD.dat', 'r');
  if (fid == -1)
    continue;
  end
  data = textscan(fid, '%f %f %f %f %f %f %f', 'CommentStyle', '#');
  fclose(fid);
  data = [data{:,:}]; % convert to normal array
  
  % If this is the first simulation of its kind, initialize variable
  if ~exist(job_type, 'var')
    eval([job_type ' = zeros( size(data,1), 2 );']);
    job_types = [job_types; {job_type}];
  end
  
  % Accumulate current data to the variable
  eval([job_type '(:,1, job_num) = data(:,2);']); % T_L
  eval([job_type '(:,2, job_num) = data(:,3);']); % k_L
  
  eval([job_type '(:,3, job_num) = data(:,4);']); % T_R
  eval([job_type '(:,4, job_num) = data(:,5);']); % k_R
  
  eval([job_type '(:,5, job_num) = data(:,6);']); % T
  eval([job_type '(:,6, job_num) = data(:,7);']); % hBD
  
  cd ..
end

hold off;


%% Plot hBD (manage each job type separately...)
figure(1);
hold all;
for job_type_index = 1 : length(job_types)
  job_type = job_types{job_type_index};
  
  data_mean = eval(['mean(' job_type ', 3);']);
  data_std = eval(['1.645 * std(' job_type ', 0, 3) / sqrt(size(' job_type ',3));']);
  
  plots(job_type_index) = ...
    errorbar(data_mean(:,5), data_mean(:,6), 2*data_std(:,6), '.');
end
hold off;

%% hBD: Plot settings

set(plots(1), 'Color', 'b', 'Marker', '.');
set(plots(2), 'Color', 'r', 'Marker', '.');
set(plots(3), 'Color', 'g', 'Marker', '.');
% set(plots(4), 'Color', 'm', 'Marker', '.');
% set(plots(4), 'Color', 'm', 'Marker', '.');

% for ii = 1 : length(job_types)
%   color = [(ii-1)/(length(job_types)-1), 0, (length(job_types)-ii)/(length(job_types)-1)];
%   set(plots(ii), 'color', color);
% end

axis([0 54 0 120]);
xlabel('{\it T},  K');
ylabel('{\it h},  MW m^{-2} K^{-1}');

hl = legend('LJ', 'Harmonic', 'Harmonic + LJ interface', ...
  'location', 'nw');
legend boxoff;

% resize markers in legend
hlm = findobj(hl, 'type', 'line');
set(hlm, 'markersize', 12);

%% hBD: Save

cd(img_dir);

print(gcf, '-dpdf', 'h_3d.pdf');

% %% Plot k_L
% figure(2);
% hold all;
% for job_type_index = 1 : length(job_types)
%   job_type = job_types{job_type_index};
%   
%   data_mean = eval(['mean(' job_type ', 3);']);
%   data_std = eval(['std(' job_type ', 0, 3);']);
%   
%   plots(job_type_index) = ...
%     errorbar(data_mean(:,1), data_mean(:,2), data_std(:,2), ...
%     'o', 'LineWidth', 1, 'MarkerSize', 5);
% end
% hold off;
% 
% %% k_L: Plot settings
% 
% axis([0 35 0 3]);
% xlabel('Temperature, K');
% ylabel('Conductivity, W m^{-1} K^{-1}');
% 
% legend(job_types, 'interpreter', 'none', 'location', 'ne');
% 
% %% k_L: Save
% 
% print(gcf, '-cmyk', '-depsc', 'k_L.eps');
% 
% %% Plot k_R
% figure(3);
% hold all;
% for job_type_index = 1 : length(job_types)
%   job_type = job_types{job_type_index};
%   
%   data_mean = eval(['mean(' job_type ', 3);']);
%   data_std = eval(['std(' job_type ', 0, 3);']);
%   
%   plots(job_type_index) = ...
%     errorbar(data_mean(:,3), data_mean(:,4), data_std(:,4), ...
%     'o', 'LineWidth', 1, 'MarkerSize', 5);
% end
% hold off;
% 
% %% k_R: Plot settings
% 
% axis([0 35 0 3]);
% xlabel('Temperature, K');
% ylabel('Conductivity, W m^{-1} K^{-1}');
% 
% legend(job_types, 'interpreter', 'none', 'location', 'ne');
% 
% %% k_R: Save
% 
% print(gcf, '-cmyk', '-depsc', 'k_R.eps');
