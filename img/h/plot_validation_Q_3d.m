clear variables;
close all;

%% Get the jobset directories

img_dir = pwd;
data_dir = '/Applications/Computation/nqlmd/research/3d_nemd/';

cd(data_dir);

% [~, more_dirs] = system('ls -d1 jobset29-r*');
% more_dirs = textscan(more_dirs, '%s');
% more_dirs = more_dirs{:};
% 
% [~, even_more_dirs] = system('ls -d1 jobset3[34]-r*');
% even_more_dirs = textscan(even_more_dirs, '%s');
% even_more_dirs = even_more_dirs{:};
% 
% dirs = [dirs; more_dirs; even_more_dirs];

%% Manual plot settings
X = 3.2;
Y = 2.5;

% Axes
set(0, 'DefaultAxesBox', 'on');
set(0, 'DefaultAxesLineWidth', 0.5);
set(0, 'DefaultAxesXMinorTick', 'on');
set(0, 'DefaultAxesYMinorTick', 'on');
set(0, 'DefaultAxesTickDir', 'in');

% Plots
set(0, 'DefaultLineLineWidth', 1.0);
set(0, 'DefaultLineMarkerSize', 12);

% Figures
set(0, 'DefaultFigurePaperSize', [X Y]);
set(0, 'DefaultFigurePaperPositionMode', 'manual');
set(0, 'DefaultFigurePaperPosition', [ 0 0 X Y ] );
set(0, 'DefaultFigurePosition', [ 0 0 X*72 Y*72 ] );

% Comment here to switch on/off Times font
set(0, 'DefaultTextFontname', 'Times New Roman', ...
       'DefaultTextFontsize', 12, ...
       'DefaultAxesFontname', 'Times New Roman', ...
       'DefaultAxesFontsize', 12 );

figure(1);
hold on;

%% Read fitted data from each run and keep running averages

data = zeros([10 2 6]);

for run = 0 : 5
  
  cd(['jobset12-r0' num2str(run) '-lj_vary_flux']);
  
  fid = fopen('hBD.dat', 'r');
  raw = textscan(fid, '%f %f %f %f %f %f %f', 'CommentStyle', '#');
  fclose(fid);
  
  data(:,:,run+1) = [raw{1} raw{7}];

  cd ..
  
end

data_ave = mean( data, 3 );
data_std = 2 * std( data, 0, 3 ) / sqrt(size(data,3));

%% Plot

h_scale = 2;
Q_scale = 0.5e9;

hold on;
errorbar(Q_scale*data_ave(:,1)*3.2e-11, h_scale*data_ave(:,2), h_scale*data_std(:,2), 'r.');

axis([0, 2, 0, 120]);
xlabel('Heat current,  nW');
ylabel('{\it{h}},  MW m^{-2} K^{-1}');

%% Annotate

annotation('textarrow', 0.525*[1,1], [0.7, 0.6], 'string', 'Final current used', 'HeadLength', 7, 'HeadWidth', 7);

text(1.3, 45, '{\it{T}} = 30 K');

%% Save

cd(img_dir);

print(gcf, '-dpdf', 'validation_Q_3d.pdf');