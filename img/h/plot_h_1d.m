clear variables;
close all;

%% Get the jobset directories

img_dir = pwd;
data_dir = '/Applications/Computation/nqlmd/research/1d_nemd/';

cd(data_dir);

[~, dirs] = system('ls -d1 jobset{28,29,30}-r*');
dirs = textscan(dirs, '%s');
dirs = dirs{:};

% [~, more_dirs] = system('ls -d1 jobset04-r*');
% more_dirs = textscan(more_dirs, '%s');
% more_dirs = more_dirs{:};

% [~, even_more_dirs] = system('ls -d1 jobset3[34]-r*');
% even_more_dirs = textscan(even_more_dirs, '%s');
% even_more_dirs = even_more_dirs{:};

% dirs = [dirs; more_dirs];

%% Manual plot settings

h_scale = 1e12;

X = 6.5;
Y = 3;

% Axes
set(0, 'DefaultAxesBox', 'on');
set(0, 'DefaultAxesLineWidth', 0.5);
set(0, 'DefaultAxesXMinorTick', 'on');
set(0, 'DefaultAxesYMinorTick', 'on');
set(0, 'DefaultAxesTickDir', 'in');

% Plots
set(0, 'DefaultLineLineWidth', 1.0);
set(0, 'DefaultLineMarkerSize', 12);

% Figures
set(0, 'DefaultFigurePaperSize', [X Y]);
set(0, 'DefaultFigurePaperPositionMode', 'manual');
set(0, 'DefaultFigurePaperPosition', [ 0 0 X Y ] );
set(0, 'DefaultFigurePosition', [ 0 0 X*72 Y*72 ] );

% Comment here to switch on/off Times font
set(0, 'DefaultTextFontname', 'Times New Roman', ...
       'DefaultTextFontsize', 12, ...
       'DefaultAxesFontname', 'Times New Roman', ...
       'DefaultAxesFontsize', 12 );

figure(1);
hold on;

%% plot theoretical hBD for the 1d harmonic system

htheory = plot([0 14], h_scale*1.3628e-11*[1 1], '--', 'LineWidth', 1, 'color', 0.5*[1,1,1]);
% job_types(size(job_types,1)+1) = {'1D elastic theory'};

%% Go into each jobset and add to the plot
job_types = {};
% % ALL
for dir_num =  1 : length(dirs)
% for dir_num = [(1:6), 1*6+(1:6), 2*6+(1:6), 3*6+(1:6)]
% % LJ vs. HARMONIC (0UC)
% for dir_num =  [(1 : 6), 24+(1 : 6 : 6*6)]
% % LJ vs. HARMONIC (0UC, 2UC)
% for dir_num =  [(1 : 6), 24+(1 : 6 : 6*6), 26+(1 : 6 : 6*6)]
% % LJ vs. HARMONIC (0UC, 2UC, ALL)
% for dir_num =  [(1 : 6), 24+(1 : 6 : 6*6), 26+(1 : 6 : 6*6), 29+(1 : 6 : 6*6)]
  
  % Move into the jobset directory
  job_dir = dirs{dir_num};
  cd(job_dir);
  
  % Determine what kind of simulation it was
  %   (from the directory name)
  temp = find(job_dir == '-', 1, 'last');
  job_type = job_dir(temp+1:end);
  
  job_num = str2double( job_dir(temp-2 : temp-1) ) + 1;
%   job_num = str2double( job_dir(temp-2 : temp-1) ) + 1 - 10;
  
  clear temp;
  
  % Load the data
  fid = fopen('hBD.dat', 'r');
  if (fid == -1)
    continue;
  end
  data = textscan(fid, '%f %f %f %f %f %f %f', 'CommentStyle', '#');
  fclose(fid);
  data = [data{:,:}]; % convert to normal array
  
  % If this is the first simulation of its kind, initialize variable
  if ~exist(job_type, 'var')
    eval([job_type ' = zeros( size(data,1), 2 );']);
    job_types = [job_types; {job_type}];
  end
  
  % Accumulate current data to the variable
  eval([job_type '(:,1, job_num) = data(:,2);']); % T_L
  eval([job_type '(:,2, job_num) = data(:,3);']); % k_L
  
  eval([job_type '(:,3, job_num) = data(:,4);']); % T_R
  eval([job_type '(:,4, job_num) = data(:,5);']); % k_R
  
  eval([job_type '(:,5, job_num) = data(:,6);']); % T
  eval([job_type '(:,6, job_num) = data(:,7);']); % hBD
  
  cd ..
end

hold off;


%% Plot hBD (manage each job type separately...)
figure(1);
hold all;

for job_type_index = 1 : length(job_types)
  job_type = job_types{job_type_index};
  
  data_mean = eval(['mean(' job_type ', 3);']);
  data_std = eval(['2 * std(' job_type ', 0, 3) / sqrt(size(' job_type ',3));']);
  
  % list of temperatures to plot
  temp_mask = (1:size(data_mean,1));
  hplots(job_type_index) = ...
    errorbar(data_mean(temp_mask,5), h_scale*data_mean(temp_mask,6), h_scale*data_std(temp_mask,6), '.');
end
hold off;

%% Try fitting a line to the LJ data at high temperatures

% temp_mask = 1 : size(lj,1);
% lj_T = mean(lj(temp_mask,5,:), 3);
% lj_h = mean(lj(temp_mask,6,:), 3);

%% hBD: Plot settings

set(hplots(1), 'Color', 'b');
set(hplots(2), 'Color', 'r');
set(hplots(3), 'Color', 'g');
% set(hplots(4), 'Color', 'm');

axis([0 14 0 150]);
ylabel('{\it h},  pW K^{-1}');
xlabel('{\it{T}},  K');

hl = legend([hplots, htheory], 'LJ', 'Harmonic', 'Harmonic w/ LJ interface', '1D harmonic theory', ...
  'location', 'nw');
legend boxoff;

% resize markers in legend
hlm = findobj(hl, 'type', 'line');
set(hlm, 'markersize', 12);

%% hBD: Save

cd(img_dir);

print(gcf, '-dpdf', 'h_1d.pdf');
%print(gcf, '-cmyk', '-dpdf', 'hBD_proposal_03.pdf');

% %% Plot k_L
% figure(2);
% hold all;
% for job_type_index = 1 : length(job_types)
%   job_type = job_types{job_type_index};
%   
%   data_mean = eval(['mean(' job_type ', 3);']);
%   data_std = eval(['std(' job_type ', 0, 3);']);
%   
%   plots(job_type_index) = ...
%     errorbar(data_mean(:,1), data_mean(:,2), data_std(:,2), ...
%     'o', 'LineWidth', 1, 'MarkerSize', 5);
% end
% hold off;
% 
% %% k_L: Plot settings
% 
% axis([0 35 0 3]);
% xlabel('Temperature, K');
% ylabel('Conductivity, W / m K');
% 
% legend(job_types, 'interpreter', 'none', 'location', 'ne');
% 
% %% k_L: Save
% 
% print(gcf, '-cmyk', '-depsc', 'k_L.eps');
% 
% %% Plot k_R
% figure(3);
% hold all;
% for job_type_index = 1 : length(job_types)
%   job_type = job_types{job_type_index};
%   
%   data_mean = eval(['mean(' job_type ', 3);']);
%   data_std = eval(['std(' job_type ', 0, 3);']);
%   
%   plots(job_type_index) = ...
%     errorbar(data_mean(:,3), data_mean(:,4), data_std(:,4), ...
%     'o', 'LineWidth', 1, 'MarkerSize', 5);
% end
% hold off;
% 
% %% k_R: Plot settings
% 
% axis([0 35 0 3]);
% xlabel('Temperature, K');
% ylabel('Conductivity, W / m K');
% 
% legend(job_types, 'interpreter', 'none', 'location', 'ne');
% 
% %% k_R: Save
% 
% print(gcf, '-cmyk', '-depsc', 'k_R.eps');
