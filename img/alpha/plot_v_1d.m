clear variables;
close all;

img_dir = pwd;
data_dir = '/Applications/Computation/nqlmd/research/1d_packets/jobset18-Ar_hAr_4xL/';

cd(data_dir);

%% Settings

q = 0.30;

z_scale = 1e1;

filename = '1d.micro';
nfields = 6;

dt = 0.001; % ps
steps_per_frame = 2000;

%% Manual plot settings
X = 3.2;
Y = 1.5;

% Axes
set(0, 'DefaultAxesBox', 'on');
set(0, 'DefaultAxesLineWidth', 0.5);
set(0, 'DefaultAxesXMinorTick', 'on');
set(0, 'DefaultAxesYMinorTick', 'on');
set(0, 'DefaultAxesTickDir', 'in');

% Plots
set(0, 'DefaultLineLineWidth', 1);
set(0, 'DefaultLineMarkerSize', 4);

% Figures
set(0, 'DefaultFigurePaperSize', [X Y]);
set(0, 'DefaultFigurePaperPositionMode', 'manual');
set(0, 'DefaultFigurePaperPosition', [ 0 0 X Y ] );
set(0, 'DefaultFigurePosition', [ 0 0 X*72 Y*72 ] );

% Comment here to switch on/off Times font
set(0, 'DefaultTextFontname', 'Times New Roman', ...
       'DefaultTextFontsize', 12, ...
       'DefaultAxesFontname', 'Times New Roman', ...
       'DefaultAxesFontsize', 12 );

%% Specify simulation directory(ies) to plot

dirs = {'T02', 'T12'};

% For each directory...
for dir_num = 1 : length(dirs)
  
  job_dir = dirs{dir_num};
  cd(sprintf('%s/q%.2f', job_dir, q));

  %% Read

  raw = load(filename);
  N = max(raw(:,1));

  nframes = numel(raw) / N / nfields;
  data = reshape(raw, [N nframes nfields]);

  m = data(:,:,3);
  r = data(:,:,4);
  v = data(:,:,5);

  %% (1) dr
  NZ = N/2;
  a = 5.313;
  r0 = (a/2) * ((0:N-1)' + 0.5);
  r0 = repmat(r0, [1 nframes]);

  dr = r - r0;

  %% (2) Fourier[dr]
  dr_fft = fft(dr);
  
  %% (3) v
  % (nothing to do)
  
  %% (4) Fourier[v]
  v_fft = abs(fft(v));

  %% Plot
  
  % Let's put a bunch of frames on the same plot.
  
  frames_to_plot = 25 : 25 : 125;
  n_interface = 2*1400;
  
  % FIGURE 1: VZ
  figure; clf;
  
  if strcmp(job_dir, 'T001')
    v_scale = 2/sqrt(0.01);
  elseif strcmp(job_dir, 'T02')
    v_scale = 2/sqrt(2);
  elseif strcmp(job_dir, 'T12')
    v_scale = 2/sqrt(12);
  else
    error('add v_scale value for this T case');
  end
  
  % Total cumulative vertical offset
  dY = -5/v_scale;
  
  % Calculations
  y_offset = linspace(0, dY, length(frames_to_plot));
  y_offset = repmat(y_offset, [size(r,1) 1]);
  
  % Plot
  hold on;
  plot((r(1:n_interface,frames_to_plot) - n_interface/2*a) / z_scale, ...
       v(1:n_interface,frames_to_plot) + y_offset(1:n_interface,:), ...
       'r-');
  plot((r(n_interface:end,frames_to_plot) - n_interface/2*a) / z_scale, ...
       sqrt(3)*v(n_interface:end,frames_to_plot) + y_offset(n_interface:end,:), ...
       'b-');
  hold off;
  
  axis([-2500/z_scale 2500/z_scale -6/v_scale 1/v_scale]);

  set(gca, 'XTick', -250 : 125 : 250);
  set(gca, 'YTick', [-5/v_scale, -2.5/v_scale, 0]);
  set(gca, 'YTickLabel', [100, 50, 0]);
  xlabel('{\it{z}}, nm');
  if (dir_num == 1)
    ylabel('{\it{t}}, ps');
  end
  
  %% Annotate with transmissivity

  R1_mask = (r(:,1)-n_interface/2*a > -2250) & (r(:,1)-n_interface/2*a < -1125);
  R2_mask = (r(:,1)-n_interface/2*a > -1125) & r(:,1)-n_interface/2*a < 0;
  T_mask = (r(:,1)-n_interface/2*a > 0) & (r(:,1)-n_interface/2*a < 1250);
  
  E = sum(m(R1_mask | R2_mask | T_mask,125) .* v(R1_mask | R2_mask | T_mask,125).^2);
  R1 = sum(m(R1_mask,125).*v(R1_mask,125).^2) / E;
  R2 = sum(m(R2_mask,125).*v(R2_mask,125).^2) / E;
  T  = sum(m(T_mask,125) .*v(T_mask,125).^2) / E;
  
  R = R1+R2;
  
  if (dir_num == 1)
    text(210, -0.55/v_scale, '(a)', 'fontsize', 9);
    text(-40, -0.6/v_scale, sprintf('$B = 0.3$ \\AA/ps'), 'fontsize', 9, 'interpreter', 'latex');
  else
    text(210, -0.55/v_scale, '(b)', 'fontsize', 9);
    text(-40, -0.6/v_scale, sprintf('$B = 0.7$ \\AA/ps'), 'fontsize', 9, 'interpreter', 'latex');
  end
  %text(-40, -0.75/v_scale, sprintf('{\\it{T}}_{equiv} = %.0f K', str2double(job_dir(2:end))), 'fontsize', 9);
  text(-220, -4.4/v_scale, sprintf('%.0f%%', 100*R1), 'fontsize', 9);
  text(-55, -4.4/v_scale, sprintf('%.0f%%', 100*R2), 'fontsize', 9);
  text(80, -4.4/v_scale, sprintf('%.0f%%', 100*T), 'fontsize', 9);
  text(-240, -3.0/v_scale, 'Ar', 'color', 0.5*[1,1,1], 'fontsize', 9);
  text(140, -3.0/v_scale, 'heavy Ar', 'color', 0.5*[1,1,1], 'fontsize', 9);
  
  %% Save files
  print(gcf, '-dpdf', sprintf('%s/v_1d_Ar_%s.pdf', img_dir, job_dir));
  
  %% Return to the jobset directory
  cd(data_dir);
  
end % loop over simulations

cd(img_dir);
