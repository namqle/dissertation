clear variables;
close all;

img_dir = pwd;
data_dir = '/Applications/Computation/nqlmd/research/1d_packets/jobset19-hAr_Ar_4xL/';

cd(data_dir);

%% Settings

q = 0.60;

z_scale = 1e1;

filename = '1d.micro';
nfields = 6;

dt = 0.001; % ps
steps_per_frame = 2000;

%% Manual plot settings
X = 3.25;
Y = 1.5;

% Axes
set(0, 'DefaultAxesBox', 'on');
set(0, 'DefaultAxesLineWidth', 0.5);
set(0, 'DefaultAxesXMinorTick', 'on');
set(0, 'DefaultAxesYMinorTick', 'on');
set(0, 'DefaultAxesTickDir', 'in');

% Plots
set(0, 'DefaultLineLineWidth', 1);
set(0, 'DefaultLineMarkerSize', 4);

% Figures
set(0, 'DefaultFigurePaperSize', [X Y]);
set(0, 'DefaultFigurePaperPositionMode', 'manual');
set(0, 'DefaultFigurePaperPosition', [ 0 0 X Y ] );
set(0, 'DefaultFigurePosition', [ 0 0 X*72 Y*72 ] );

% Comment here to switch on/off Times font
set(0, 'DefaultTextFontname', 'Times New Roman', ...
       'DefaultTextFontsize', 12, ...
       'DefaultAxesFontname', 'Times New Roman', ...
       'DefaultAxesFontsize', 12 );

%% Specify simulation directory(ies) to plot

dirs = {'T02', 'T12'};

% For each directory...
for dir_num = 1 : length(dirs)
  
  job_dir = dirs{dir_num};
  cd(sprintf('%s/q%.2f', job_dir, q));

  %% Read

  raw = load(filename);
  N = max(raw(:,1));

  nframes = numel(raw) / N / nfields;
  data = reshape(raw, [N nframes nfields]);

  m = data(:,:,3);
  r = data(:,:,4);
  v = data(:,:,5);

  %% (1) dr
  NZ = N/2;
  a = 5.313;
  r0 = (a/2) * ((0:N-1)' + 0.5);
  r0 = repmat(r0, [1 nframes]);

  dr = r - r0;

  %% (2) Fourier[dr]
  dr_fft = fft(dr);
  
  %% (3) v
  % (nothing to do)
  
  %% (4) Fourier[v]
  v_fft = abs(fft(v));

  %% Plot
  
  % Let's put a bunch of frames on the same plot.
  
  frames_to_plot = 150 : 25 : 250;
  n_interface = 2*1000;
  
  % FIGURE 1: VZ
  figure(1); clf;
  
  if strcmp(job_dir, 'T001')
    v_scale = 12/sqrt(0.01);
  elseif strcmp(job_dir, 'T02')
    v_scale = 12/sqrt(2);
  elseif strcmp(job_dir, 'T12')
    v_scale = 12/sqrt(12);
  else
    error('add v_scale value for this T case');
  end
  
  % Total cumulative vertical offset
  dY = -5/v_scale;
  
  % Calculations
  y_offset = linspace(0, dY, length(frames_to_plot));
  y_offset = repmat(y_offset, [size(r,1) 1]);
  
  % Plot
  hold on;
  plot((r(1:n_interface,frames_to_plot) - n_interface/2*a) / z_scale, ...
       v(1:n_interface,frames_to_plot)/sqrt(3) + y_offset(1:n_interface,:), ...
       'b-');
  plot((r(n_interface:end,frames_to_plot) - n_interface/2*a) / z_scale, ...
       v(n_interface:end,frames_to_plot)/sqrt(3) + y_offset(n_interface:end,:), ...
       'r-');
  hold off;
  
  axis([-220*a/z_scale 480*a/z_scale -6/v_scale 1/v_scale]);

  set(gca, 'YTick', [-5/v_scale 0]);
  set(gca, 'YTickLabel', [100 0]);
  xlabel('{\it{z}}, nm');
  ylabel('{\it{t}}, ps');
  
  
  
%   % FIGURE 2: FFT[VZ]
%   figure(2);
%   
%   % The total cumulative offset
%   dY = -50;
%   
%   % Calculations
%   y_offset = linspace(0, dY, length(frames_to_plot));
%   y_offset = repmat(y_offset, [size(r,1) 1]);
%   
%   % Plot
%   plot(r(1:end/2, frames_to_plot)./r(end/2, 1), ...
%        v_fft(1:end/2, frames_to_plot) + y_offset(1:end/2, :), ...
%        'm-', 'LineWidth', 1);
%   
%   axis([0 1 [-1.2*abs(dY) 0.6*abs(dY)]]);
%   xlabel('q / q_{max}');
%   ylabel('FFT[v]');
  
  %% Save files
  print(figure(1), '-dpdf', sprintf('%s/v_1d_hAr_%s.pdf', img_dir, job_dir));
  
  %% Return to the jobset directory
  cd(data_dir);
  
end % loop over simulations

cd(img_dir);
