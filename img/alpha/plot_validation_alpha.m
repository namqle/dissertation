clear variables;
close all;

img_dir = pwd;
data_dir = '/Applications/Computation/nqlmd/research/1d_packets';

aap2Trads = 98.2291;

%% Manual plot settings
X = 3.2;
Y = 2.5;

% Line Plots
set(0, 'DefaultLineLineWidth', 0.5);
set(0, 'DefaultLineMarkerSize', 9);

% Surface Plots
set(0, 'DefaultSurfaceFaceColor', 'interp');
set(0, 'DefaultSurfaceEdgeColor', 'none');
% set(0, 'DefaultAxisColor', [0 0 0.5625]);

% Figures
set(0, 'DefaultFigurePaperSize', [X Y]);
set(0, 'DefaultFigurePaperPositionMode', 'manual');
set(0, 'DefaultFigurePaperPosition', [ 0 0 X Y ] );
set(0, 'DefaultFigurePosition', [ 0 0 X*72 Y*72 ] );

% Comment here to switch on/off Times font
set(0, 'DefaultTextFontname', 'Times New Roman', ...
       'DefaultTextFontsize', 12, ...
       'DefaultAxesFontname', 'Times New Roman', ...
       'DefaultAxesFontsize', 12 );

%% Plot analytical solution (Saltonstall2013)

figure; hold on;

k = 0.16498; % eV/A^2
m1 = 40;
m2 = 120;

ww = 2*pi*linspace(0, 1.5, 100) / aap2Trads;
Gamma1 = real(2*ww.*sqrt(m1*k - (m1*ww/2).^2));
Gamma2 = real(2*ww.*sqrt(m2*k - (m2*ww/2).^2));

alpha_salt = 4*Gamma1.*Gamma2 ./ ((Gamma1+Gamma2).^2 + ww.^4*(m1-m2)^2);

h_salt = plot(ww*aap2Trads/(2*pi), alpha_salt, '-', 'color', 0.3*[1,1,1]);

%% Loop over "temperatures"

jobset_list = {'jobset18-Ar_hAr_4xL', 'jobset19-hAr_Ar_4xL'};
for i_jobset_list = 1 : length(jobset_list);
  
  jobset = jobset_list{i_jobset_list};
  
  cd(sprintf('%s/%s/harmonic', data_dir, jobset));
  
  %% Load data from plaintext
  
  folders = dir('q*');
  folders = {folders.name};
  
  N_q_out = 4097; % number of discretized q_out points
  
  q_in = zeros(size(folders,2), N_q_out);
  q_out = zeros(size(folders,2), N_q_out);
  
  T_q = zeros(size(q_in,1), size(q_out,2));
  R_q = zeros(size(q_in,1), size(q_out,2));
  
  for index = 1 : size(folders,2)
    
    folder = folders{index};
    
    cd(folder);
    
    q_in(index, :) = str2double(folder(2:end));
    
    raw_data = load('transmission.dat');
    
    if (size(raw_data,1) == N_q_out)
      
      q_out(index, :) = raw_data(:,1)';
      R_q(index, :) = raw_data(:,2)';
      T_q(index, :) = raw_data(:,3)';
      
    else
      
      error(['uh oh -- ' folder '/transmission.dat looks different than I expect']);
      
    end
    
    cd ..;
  end
  
  %% Translate q -> w
  
  w = @(q, k, m) (4*k/m)^0.5 * sin(pi*q/2) * aap2Trads; % in rad/s
  f = @(q, k, m) (4*k/m)^0.5 * sin(pi*q/2) * aap2Trads / (2*pi); % in Hz
  
  %% Plot total values
  
  % Total transmissivity
  delta_q = 2*pi/30;
  
  if i_jobset_list == 1
    linespec = 'rx';
    error = delta_q * 0.25 * (4*k/m1)^0.5 * cos(pi*q_in(:,1)/2) * aap2Trads / (2*pi);
    h1 = plot(f(q_in(:,1), k, m1), sum(T_q,2), linespec);
    herrorbar(f(q_in(18,1), k, m1), sum(T_q(18,:),2), error(18), linespec);
  else
    linespec = 'b.';
    error = delta_q * 0.25 * (4*k/m2)^0.5 * cos(pi*q_in(:,1)/2) * aap2Trads / (2*pi);
    h2 = plot(f(q_in(1:16,1), k, m2), sum(T_q(1:16,:),2), linespec);
    herrorbar(f(q_in(17,1), k, m2), sum(T_q(17,:),2), error(17), linespec);
  end
  
  axis([0 1.5 0 1.0]);
  set(gca, 'YTick', 0 : 0.2 : 1.0);
  
  xlabel('{\it{ f }},  THz');
  ylabel('{\it{ \alpha }}( {\it{f}} )');
  
end

%% Label and print

cd(img_dir);

hl = legend([h1, h2, h_salt], 'Ar {\rightarrow} heavy Ar', ...
                              'heavy Ar {\rightarrow} Ar', ...
                              'Theory (Eq. 4.5)', ...
                              'location', 'sw');
legend boxoff;
set(hl, 'fontsize', 10);

print(gcf, '-dpdf', 'validation_alpha.pdf');