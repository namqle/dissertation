clear variables;
close all;

current_dir = pwd;
data_dir = '/Applications/Computation/nqlmd/research/1d_packets/jobset20-Ar_hAr_10xM/';

cmax = 0.08;

%% Manual plot settings
X = 6.5;
Y = 8;

% Line Plots
set(0, 'DefaultLineLineWidth', 0.5);
set(0, 'DefaultLineMarkerSize', 16);

% Surface Plots
set(0, 'DefaultSurfaceFaceColor', 'interp');
set(0, 'DefaultSurfaceEdgeColor', 'none');
set(0, 'DefaultFigureColormap', redmap(128));
% set(0, 'DefaultAxisColor', [0 0 0.5625]);

% Figures
set(0, 'DefaultFigurePaperSize', [X Y]);
set(0, 'DefaultFigurePaperPositionMode', 'manual');
set(0, 'DefaultFigurePaperPosition', [ 0 0 X Y ] );
set(0, 'DefaultFigurePosition', [ 0 0 X*72 Y*72 ] );

% Comment here to switch on/off Times font
set(0, 'DefaultTextFontname', 'Times New Roman', ...
       'DefaultTextFontsize', 12, ...
       'DefaultAxesFontname', 'Times New Roman', ...
       'DefaultAxesFontsize', 12 );

%% Loop over "temperatures"
  
figure;

gap = [0.05, 0.04];
marg_h = [0.08, 0.02];
marg_w = [0.09, 0.09];

ha = tight_subplot(3, 2, gap, marg_h, marg_w);
for ii = 1 : length(ha)
  hold(ha(ii), 'on');
  set(ha(ii), 'XTickLabelMode', 'Auto', 'YTickLabelMode', 'Auto');
end

T_list = [2, 12];
for i_T_list = 1 : length(T_list);
  
  T = T_list(i_T_list);
  
  cd(sprintf('%s/T%02d', data_dir, T));
  
  %% Load data from plaintext
  
  folders = dir('q*');
  folders = {folders.name};
  
  N_q_out = 4097; % number of discretized q_out points
  
  q_in = zeros(size(folders,2), N_q_out);
  q_out = zeros(size(folders,2), N_q_out);
  
  T_q = zeros(size(q_in,1), size(q_out,2));
  R_q = zeros(size(q_in,1), size(q_out,2));
  
  for index = 1 : size(folders,2)
    
    folder = folders{index};
    
    cd(folder);
    
    q_in(index, :) = str2double(folder(2:end));
    
    raw_data = load('transmission.dat');
    
    if (size(raw_data,1) == N_q_out)
      
      q_out(index, :) = raw_data(:,1)';
      R_q(index, :) = raw_data(:,2)';
      T_q(index, :) = raw_data(:,3)';
      
    else
      
      error(['uh oh -- ' folder '/transmission.dat looks different than I expect']);
      
    end
    
    cd ..;
  end
  
  %% Translate q -> w
  
  w = @(q, k, m) (4*k/m)^0.5 * sin(pi*q/2) * 98.2291; % in rad/s
  f = @(q, k, m) (4*k/m)^0.5 * sin(pi*q/2) * 98.2291 / (2*pi); % in Hz
  
  %% Plot in frequency space
  
  k = 0.16498; % eV/A^2
  m1 = 40;
  m2 = 400;
  
  plot(  ha(2*i_T_list-1), [0 1.5], [0 1.5], '-', 'Color', 0.8*[1 1 1]);
  plot(  ha(2*i_T_list-1), [0 1.5], [0 3.0], '-', 'Color', 0.8*[1 1 1]);
  pcolor(ha(2*i_T_list-1), f(q_in, k, m1), f(q_out, k, m1), R_q);
  axis(  ha(2*i_T_list-1), [0 1.5 0 2.0]);
  caxis( ha(2*i_T_list-1), [0 cmax]);
  ylabel(ha(2*i_T_list-1), 'Reflected {\it{ f }}_{out}, (THz)');
  
  plot(  ha(2*i_T_list), [0 1.5], [0 1.5], '-', 'Color', 0.8*[1 1 1]);
  plot(  ha(2*i_T_list), [0 1.5], [0 3.0], '-', 'Color', 0.8*[1 1 1]);
  pcolor(ha(2*i_T_list), f(q_in, k, m1), f(q_out, k, m2), T_q);
  axis(  ha(2*i_T_list), [0 1.5 0 2.0]);
  caxis( ha(2*i_T_list), [0 cmax]);
  ylabel(ha(2*i_T_list), 'Transmitted {\it{ f }}_{out}, (THz)');
  
  %% Plot total values
  
  if i_T_list == 1
    color = 'b';
  else
    color = 'r';
  end
  
  % Total reflectivity
  plot(ha(5), f(q_in(:,1), k, m1), sum(R_q,2), '.', 'color', color);
  axis(ha(5), [0 1.5 0 1.0]);
  set( ha(5), 'YTick', 0 : 0.2 : 1.0);
  
  xlabel(ha(5), 'Incoming {\it{ f }}, (THz)');
  ylabel(ha(5), 'Reflectivity 1 - {\it{ \alpha }}');
  
  % Total transmissivity
  plot(ha(6), f(q_in(:,1), k, m1), sum(T_q,2), '.', 'color', color);
  axis(ha(6), [0 1.5 0 1.0]);
  set( ha(6), 'YTick', 0 : 0.2 : 1.0);
  
  xlabel(ha(6), 'Incoming {\it{ f }}_{in}, (THz)');
  ylabel(ha(6), 'Transmissivity {\it{ \alpha }}');
  
end

%% Label and print

cd(current_dir);

% Put a colorbar in one of the plots
axes(ha(2));
hcbar = colorbar('north');
% Shrink it a bit
cpos = get(hcbar, 'Position');
cpos(3) = 0.96*cpos(3);
cpos(4) = 0.6*cpos(4);
set(hcbar, 'Position', cpos);

% Flip y-axis labels to the right
for ii = 2 : 2 : 6
  set(ha(ii), 'YAxisLocation', 'right');
end

print(gcf, '-dpng', '-r600', 'alpha_1d_Ar_hAr_10xM.png');