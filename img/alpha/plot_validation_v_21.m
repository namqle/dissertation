clear variables;
close all;

img_dir = pwd;
data_dir = '/Applications/Computation/nqlmd/research/1d_packets/jobset19-hAr_Ar_4xL/';

cd(data_dir);

%% Settings

q = 0.60;

z_scale = 1e1;

filename = '1d.micro';
nfields = 6;

dt = 0.001; % ps
steps_per_frame = 2000;

%% Manual plot settings
X = 3.2;
Y = 1.5;

% Axes
set(0, 'DefaultAxesBox', 'on');
set(0, 'DefaultAxesLineWidth', 0.5);
set(0, 'DefaultAxesXMinorTick', 'on');
set(0, 'DefaultAxesYMinorTick', 'on');
set(0, 'DefaultAxesTickDir', 'in');

% Plots
set(0, 'DefaultLineLineWidth', 1);
set(0, 'DefaultLineMarkerSize', 4);

% Figures
set(0, 'DefaultFigurePaperSize', [X Y]);
set(0, 'DefaultFigurePaperPositionMode', 'manual');
set(0, 'DefaultFigurePaperPosition', [ 0 0 X Y ] );
set(0, 'DefaultFigurePosition', [ 0 0 X*72 Y*72 ] );

% Comment here to switch on/off Times font
set(0, 'DefaultTextFontname', 'Times New Roman', ...
       'DefaultTextFontsize', 12, ...
       'DefaultAxesFontname', 'Times New Roman', ...
       'DefaultAxesFontsize', 12 );

%% Specify simulation directory(ies) to plot

dirs = {'harmonic'};

% For each directory...
for dir_num = 1 : length(dirs)
  
  job_dir = dirs{dir_num};
  cd(sprintf('%s/q%.2f', job_dir, q));

  %% Read

  raw = load(filename);
  N = max(raw(:,1));

  nframes = numel(raw) / N / nfields;
  data = reshape(raw, [N nframes nfields]);

  m = data(:,:,3);
  r = data(:,:,4);
  v = data(:,:,5);

  %% (1) dr
  NZ = N/2;
  a = 5.313;
  r0 = (a/2) * ((0:N-1)' + 0.5);
  r0 = repmat(r0, [1 nframes]);

  dr = r - r0;

  %% (2) Fourier[dr]
  dr_fft = fft(dr);
  
  %% (3) v
  % (nothing to do)
  
  %% (4) Fourier[v]
  v_fft = abs(fft(v));

  %% Plot
  
  % Let's put a bunch of frames on the same plot.
  
  frames_to_plot = 150 : 25 : 250;
  n_interface = 2*1000;
  
  % FIGURE 1: VZ
  figure(1); clf;
  
  if strcmp(job_dir, 'T001') || strcmp(job_dir, 'harmonic')
    v_scale = 4/sqrt(0.01);
  elseif strcmp(job_dir, 'T02')
    v_scale = 4/sqrt(2);
  elseif strcmp(job_dir, 'T12')
    v_scale = 4/sqrt(12);
  else
    error('add v_scale value for this T case');
  end
  
  % Total cumulative vertical offset
  dY = -5/v_scale;
  
  % Calculations
  y_offset = linspace(0, dY, length(frames_to_plot));
  y_offset = repmat(y_offset, [size(r,1) 1]);
  
  % Plot
  hold on;
  plot((r(1:n_interface,frames_to_plot) - n_interface/2*a) / z_scale, ...
       v(1:n_interface,frames_to_plot) + y_offset(1:n_interface,:), ...
       'b-');
  plot((r(n_interface:end,frames_to_plot) - n_interface/2*a) / z_scale, ...
       v(n_interface:end,frames_to_plot) + y_offset(n_interface:end,:), ...
       'r-');
  hold off;
  
  axis([-2500/z_scale 2500/z_scale -6/v_scale 1/v_scale]);

  set(gca, 'XTick', -250 : 125 : 250);
  set(gca, 'YTick', [-5/v_scale, -2.5/v_scale, 0]);
  set(gca, 'YTickLabel', [100 50 0]);
  xlabel('{\it{z}}, nm');
  ylabel(' ');
  
  %% Annotate with transmissivity
  
  raw = load('transmission.dat');
  qq = raw(:,1);
  Rq = raw(:,2);
  Tq = raw(:,3);
  
  clear raw;
  
  R = sum(Rq) / (sum(Rq)+sum(Tq));
  T = sum(Tq) / (sum(Rq)+sum(Tq));
  
  text(210, -0.55/v_scale, '(b)', 'fontsize', 9);
  text(-200, -0.75/v_scale, sprintf('{\\it{q}}_0 = %.1f {\\it{q}}_{max}', q), 'fontsize', 9);
  text(-140, -4.4/v_scale, sprintf('%.0f%%', 100*R), 'fontsize', 9);
  text(30, -4.4/v_scale, sprintf('%.0f%%', 100*T), 'fontsize', 9);
  text(-240, -3.0/v_scale, 'heavy Ar', 'color', 0.5*[1,1,1], 'fontsize', 9);
  text(210, -3.0/v_scale, 'Ar', 'color', 0.5*[1,1,1], 'fontsize', 9);
  
  %% Save files
  print(figure(1), '-dpdf', sprintf('%s/validation_v_21.pdf', img_dir));
  
  %% Return to the jobset directory
  cd(data_dir);
  
end % loop over simulations

cd(img_dir);
