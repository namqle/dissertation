clear variables;
close all;

img_dir = pwd;
data_dir = '/Applications/Computation/nqlmd/research/3d_packets/jobset01-Ar_hAr_013_2/';

cd(data_dir);

%% Settings

% q_list = [0.10, 0.20, 0.30, 0.35, 0.40, 0.45];
q_list = [0.40, 0.45];
a = 5.313;

Nx = 30;
Ny = 30;
Nz = 60;
Nz_interface = 40;

z_scale = 1e1;

filename = '3d.micro';
nfields = 9;

dt = 0.004; % ps
steps_per_frame = 500;

%% Manual plot settings
X = 3.2;
Y = 1.5;

% Axes
set(0, 'DefaultAxesBox', 'on');
set(0, 'DefaultAxesLineWidth', 0.5);
set(0, 'DefaultAxesXMinorTick', 'on');
set(0, 'DefaultAxesYMinorTick', 'on');
set(0, 'DefaultAxesTickDir', 'in');

% Plots
set(0, 'DefaultLineLineWidth', 1);
set(0, 'DefaultLineMarkerSize', 4);

% Figures
set(0, 'DefaultFigurePaperSize', [X Y]);
set(0, 'DefaultFigurePaperPositionMode', 'manual');
set(0, 'DefaultFigurePaperPosition', [ 0 0 X Y ] );
set(0, 'DefaultFigurePosition', [ 0 0 X*72 Y*72 ] );

% Comment here to switch on/off Times font
set(0, 'DefaultTextFontname', 'Times New Roman', ...
       'DefaultTextFontsize', 12, ...
       'DefaultAxesFontname', 'Times New Roman', ...
       'DefaultAxesFontsize', 12 );

%% Specify simulation directory(ies) to plot

% dirs = {'harmonic_T50', 'T10', 'T30', 'T50'};
dirs = {'T10', 'T30', 'T50'};

% For each directory...
for dir_num = 1 : length(dirs)
  
  job_dir = dirs{dir_num};
  cd(job_dir);
  
  % Loop over q
  for iq = 1 : length(q_list)
    
    q = q_list(iq);
    
    cd(sprintf('q%.2f', q));
    
    %% Read file
    
    % read micro file
    raw = load(filename);
    N = max(raw(:,1)) + 1;
    
    nframes = numel(raw) / N / nfields;
    data = reshape(raw, [N nframes nfields]);
    
    m = data(:,:,3);
    v = data(:,:,7:9);
    
    % get equilibrium positions from crystal.init
    fid = fopen('crystal.init','r');
    N = textscan(fid, '%d', 1, 'CommentStyle', '#');
    N = N{1};
    
    raw = textscan(fid, '%f %f %f %f %f %f %f %f %f', 'CommentStyle', '#');
    r_eq = [raw{4} raw{5} raw{6}];
    
    % put origin at center of interface
    r_eq(:,1) = r_eq(:,1) - Nx/2*a;
    r_eq(:,2) = r_eq(:,2) - Ny/2*a;
    r_eq(:,3) = r_eq(:,3) - Nz_interface*a;
    
    fclose(fid);
    
    %% Plot velocities
    
    close all;
    
    % mask to pick out "central slice" of atoms
    mask = (r_eq(:,1) > -0.5*a) & (r_eq(:,1) < 0.5*a);
    
    % plot settings
    pointsize = 2;
    frames = [1, 9];
    
    
    if strcmp(job_dir, 'harmonic')
      clim = 0.3*[-1, 1];
    elseif strcmp(job_dir, 'T10')
      clim = sqrt(5)*0.3*[-1, 1];
    elseif strcmp(job_dir, 'T30')
      clim = sqrt(15)*0.3*[-1, 1];
    elseif strcmp(job_dir, 'T50') || strcmp(job_dir, 'harmonic_T50')
      clim = sqrt(25)*0.3*[-1, 1];
    end
    
    for iframe = 1 : size(frames,2)
      figure; hold on;
      colormap(pmkmp(192,'edge'));
      
      nframe = frames(iframe);
      %     scatter(r_eq(mask,3)/z_scale, r_eq(mask,2)/z_scale, pointsize, sqrt(m(mask,1)).*v(mask,nframe,1), 'filled');
      scatter(r_eq(mask,3)/z_scale, r_eq(mask,2)/z_scale, pointsize, v(mask,nframe,1), 'filled');
      plot([0,0], 10*[-1,1], '-', 'color', 0.8*[1,1,1], 'linewidth', 0.5);
      
      xlabel(' ');
      ylabel('{\it{y}},  nm');
      
      axis([-20, 20, -10, 10]);
      caxis(clim);
      
      if iframe ~= size(frames,2)
        set(gca, 'XTickLabel', '');
      end
      
      if iframe == size(frames,2)
        
        % label x axis
        xlabel('{\it{z}},  nm');
        
        % label materials
        text(-19, -8, 'Ar', 'color', 0.4*[1,1,1], 'fontsize', 10);
        text(10, -8, 'heavy Ar', 'color', 0.4*[1,1,1], 'fontsize', 10);
        
        %% annotate with reflectivity/transmissivity
        E = m(:,nframe) .* sum(v(:,nframe,:).^2, 3);
        
        mask_R = r_eq(:,3) < 0;
        mask_T = r_eq(:,3) > 0;
        
        R = sum(E(mask_R)) / sum(E);
        T = sum(E(mask_T)) / sum(E);
        
        text(-17, 0, sprintf('%.0f%%', 100*R));
        text(7, 0, sprintf('%.0f%%', 100*T));
      end
      
      
      %% Save file
      print(gcf, '-dpdf', sprintf('%s/alpha_3d/v_3d_Ar_pol2_%s_q%.2f_%d.pdf', img_dir, job_dir, q, iframe));
      
    end
    
    cd ..;
    
  end
  
  %% Return to the jobset directory
  cd(data_dir);
  
end % loop over simulations

cd(img_dir);
