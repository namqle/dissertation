clear variables;
close all;

current_dir = pwd;
data_dir = '/Applications/Computation/nqlmd/research/1d_packets/jobset19-hAr_Ar_4xL/';

cmax = 0.02;

%% Manual plot settings
X = 6.5;
Y = 8;

% Line Plots
set(0, 'DefaultLineLineWidth', 0.5);
set(0, 'DefaultLineMarkerSize', 9);

% Surface Plots
set(0, 'DefaultSurfaceFaceColor', 'interp');
set(0, 'DefaultSurfaceEdgeColor', 'none');
set(0, 'DefaultFigureColormap', redmap(128));
% set(0, 'DefaultAxisColor', [0 0 0.5625]);

% Figures
set(0, 'DefaultFigurePaperSize', [X Y]);
set(0, 'DefaultFigurePaperPositionMode', 'manual');
set(0, 'DefaultFigurePaperPosition', [ 0 0 X Y ] );
set(0, 'DefaultFigurePosition', [ 0 0 X*72 Y*72 ] );

% Comment here to switch on/off Times font
set(0, 'DefaultTextFontname', 'Times New Roman', ...
       'DefaultTextFontsize', 12, ...
       'DefaultAxesFontname', 'Times New Roman', ...
       'DefaultAxesFontsize', 12 );

%% Set up subplots
  
figure;

gap = [0.05, 0.04];
marg_h = [0.08, 0.02];
marg_w = [0.09, 0.09];

ha = tight_subplot(3, 2, gap, marg_h, marg_w);
for ii = 1 : length(ha)
  hold(ha(ii), 'on');
  set(ha(ii), 'XTickLabelMode', 'Auto', 'YTickLabelMode', 'Auto');
end

%% Plot analytical solution (Saltonstall2013)

aap2Trads = 98.2291;

k = 0.16498; % eV/A^2
m1 = 120;
m2 = 40;

ww = 2*pi*linspace(0, 1.5, 100) / aap2Trads;
Gamma1 = real(2*ww.*sqrt(m1*k - (m1*ww/2).^2));
Gamma2 = real(2*ww.*sqrt(m2*k - (m2*ww/2).^2));

alpha_salt = 4*Gamma1.*Gamma2 ./ ((Gamma1+Gamma2).^2 + ww.^4*(m1-m2)^2);

h_salt_R = plot(ha(5), ww*aap2Trads/(2*pi), 1-alpha_salt, '-', 'color', 0.3*[1,1,1]);
h_salt_T = plot(ha(6), ww*aap2Trads/(2*pi),   alpha_salt, '-', 'color', 0.3*[1,1,1]);

%% Loop over "temperatures"

T_list = [2, 12];
for i_T_list = 1 : length(T_list);
  
  T = T_list(i_T_list);
  
  cd(sprintf('%s/T%02d', data_dir, T));
  
  %% Load data from plaintext
  
  folders = dir('q*');
  folders = {folders.name};
  
  N_q_out = 4097; % number of discretized q_out points
  
  q_in = zeros(size(folders,2), N_q_out);
  q_out = zeros(size(folders,2), N_q_out);
  
  T_q = zeros(size(q_in,1), size(q_out,2));
  R_q = zeros(size(q_in,1), size(q_out,2));
  
%   for index = 1 : size(folders,2)
  for index = 1 : size(folders,2)
    
    folder = folders{index};
    
    cd(folder);
    
    q_in(index, :) = str2double(folder(2:end));
    
    raw_data = load('transmission.dat');
    
    if (size(raw_data,1) == N_q_out)
      
      q_out(index, :) = raw_data(:,1)';
      R_q(index, :) = raw_data(:,2)';
      T_q(index, :) = raw_data(:,3)';
      
    else
      
      error(['uh oh -- ' folder '/transmission.dat looks different than I expect']);
      
    end
    
    cd ..;
  end
  
  %% Translate q -> w
  
  w = @(q, k, m) (4*k/m)^0.5 * sin(pi*q/2) * 98.2291; % in rad/s
  f = @(q, k, m) (4*k/m)^0.5 * sin(pi*q/2) * 98.2291 / (2*pi); % in Hz
  
  %% Plot in frequency space
  
  plot(  ha(2*i_T_list-1), [0 1.5], [0 1.5], '-', 'Color', 0.8*[1 1 1]);
  plot(  ha(2*i_T_list-1), [0 1.5], [0 3.0], '-', 'Color', 0.8*[1 1 1]);
  plot(  ha(2*i_T_list-1), [0 1.5], [0 4.5], '-', 'Color', 0.8*[1 1 1]);
  pcolor(ha(2*i_T_list-1), f(q_in, k, m1), f(q_out, k, m1), R_q);
  axis(  ha(2*i_T_list-1), [0 1.5 0 2.0]);
  caxis( ha(2*i_T_list-1), [0 cmax]);
  ylabel(ha(2*i_T_list-1), 'Reflected { \it{f }} '', THz');
  
  plot(  ha(2*i_T_list), [0 1.5], [0 1.5], '-', 'Color', 0.8*[1 1 1]);
  plot(  ha(2*i_T_list), [0 1.5], [0 3.0], '-', 'Color', 0.8*[1 1 1]);
  plot(  ha(2*i_T_list), [0 1.5], [0 4.5], '-', 'Color', 0.8*[1 1 1]);
  pcolor(ha(2*i_T_list), f(q_in, k, m1), f(q_out, k, m2), T_q);
  axis(  ha(2*i_T_list), [0 1.5 0 2.0]);
  caxis( ha(2*i_T_list), [0 cmax]);
  ylabel(ha(2*i_T_list), 'Transmitted { \it{f }} '', THz');
  
  %% Plot total values
  
  if i_T_list == 1
    linespec = 'bx';
  else
    linespec = 'r.';
  end
  
  mask = 1:16;
  
  % Total reflectivity
  plot(ha(5), f(q_in(mask,1), k, m1), sum(R_q(mask,:),2), linespec);
  axis(ha(5), [0 1.5 0 1.0]);
  set( ha(5), 'YTick', 0 : 0.2 : 1.0);
  
  xlabel(ha(5), 'Initial $f_0$, THz', 'interpreter', 'latex');
  ylabel(ha(5), '1 - {\it{\alpha}} ( {\it{f}} )');
  
  % Total transmissivity
  plot(ha(6), f(q_in(mask,1), k, m1), sum(T_q(mask,:),2), linespec);
  axis(ha(6), [0 1.5 0 1.0]);
  set( ha(6), 'YTick', 0 : 0.2 : 1.0);
  
  xlabel(ha(6), 'Incoming $f_0$, THz', 'interpreter', 'latex');
  ylabel(ha(6), '{\it{\alpha}} ( {\it{f}} )');
  
end

%% Label and print

cd(current_dir);

% Label subplots
for ii = 1 : 4
  axes(ha(ii));
  text(0.1, 1.8, sprintf('(%s)', char(96+ii)));
end
axes(ha(5));
text(1.3, 0.1, '(e)');
axes(ha(6));
text(0.1, 0.1, '(f)');

% Label f_0 = n f' lines
axes(ha(4));
text(1.1, 1.4, '$f'' = f_0$', 'interpreter', 'latex');
text(0.8, 1.8, '$f'' = 2 f_0$', 'interpreter', 'latex');
text(0.3, 1.7, '$f'' = 3 f_0$', 'interpreter', 'latex');

% Put a colorbar in one of the plots
axes(ha(2));
hcbar = colorbar('east');
% Push it around a bit
cpos = get(hcbar, 'Position');
cpos(1) = 1.01*cpos(1);
cpos(3) = 0.5*cpos(3);
set(hcbar, 'Position', cpos);
set(hcbar, 'fontsize', 9);

% Flip y-axis labels to the right
for ii = 2 : 2 : 6
  set(ha(ii), 'YAxisLocation', 'right');
end

% Label amplitudes
axes(ha(1));
text(0.8, 0.2, '{\it{T}}_{equiv} = 2 K');
axes(ha(3));
text(0.8, 0.2, '{\it{T}}_{equiv} = 12 K');

% Legends on total reflectivity/transmissivity
hl = legend(ha(5), 'Harmonic (Eq. 4.5)', '{\it{T}}_{equiv} = 2 K', '{\it{T}}_{equiv} = 12 K');
set(hl, 'location', 'northwest', 'fontsize', 10, 'box', 'off');

print(gcf, '-dpng', '-r600', 'alpha_1d_hAr_Ar.png');