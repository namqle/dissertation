clear variables;
close all;

current_dir = pwd;
data_dir = '/Applications/Computation/nqlmd/research/1d_packets/jobset18-Ar_hAr_4xL/';

cmin = -1e-10;
cmax = 0.025;

%% Manual plot settings
X = 6.5;
Y = 2.5;

% Line Plots
set(0, 'DefaultLineLineWidth', 0.5);
set(0, 'DefaultLineMarkerSize', 9);

% Surface Plots
set(0, 'DefaultSurfaceFaceColor', 'interp');
set(0, 'DefaultSurfaceEdgeColor', 'none');
set(0, 'DefaultFigureColormap', redmap(128));
% set(0, 'DefaultAxisColor', [0 0 0.5625]);

% Figures
set(0, 'DefaultFigurePaperSize', [X Y]);
set(0, 'DefaultFigurePaperPositionMode', 'manual');
set(0, 'DefaultFigurePaperPosition', [ 0 0 X Y ] );
set(0, 'DefaultFigurePosition', [ 0 0 X*72 Y*72 ] );

% Comment here to switch on/off Times font
set(0, 'DefaultTextFontname', 'Times New Roman', ...
       'DefaultTextFontsize', 12, ...
       'DefaultAxesFontname', 'Times New Roman', ...
       'DefaultAxesFontsize', 12 );

%% Loop over "temperatures"
  
figure;

gap = [0.05, 0.04];
marg_h = [0.2, 0.02];
marg_w = [0.09, 0.09];

ha = tight_subplot(1, 2, gap, marg_h, marg_w);
for ii = 1 : length(ha)
  hold(ha(ii), 'on');
  set(ha(ii), 'XTickLabelMode', 'Auto', 'YTickLabelMode', 'Auto');
end

T_list = {'harmonic'};
for i_T_list = 1 : length(T_list);
  
  T = T_list{i_T_list};
  
  cd(sprintf('%s/%s', data_dir, T));
  
  %% Load data from plaintext
  
  folders = dir('q*');
  folders = {folders.name};
  
  N_q_out = 4097; % number of discretized q_out points
  
  q_in = zeros(size(folders,2), N_q_out);
  q_out = zeros(size(folders,2), N_q_out);
  
  T_q = zeros(size(q_in,1), size(q_out,2));
  R_q = zeros(size(q_in,1), size(q_out,2));
  
  for index = 1 : size(folders,2)
    
    folder = folders{index};
    
    cd(folder);
    
    q_in(index, :) = str2double(folder(2:end));
    
    raw_data = load('transmission.dat');
    
    if (size(raw_data,1) == N_q_out)
      
      q_out(index, :) = raw_data(:,1)';
      R_q(index, :) = raw_data(:,2)';
      T_q(index, :) = raw_data(:,3)';
      
    else
      
      error(['uh oh -- ' folder '/transmission.dat looks different than I expect']);
      
    end
    
    cd ..;
  end
  
  %% Translate q -> w
  
  w = @(q, k, m) (4*k/m)^0.5 * sin(pi*q/2) * 98.2291; % in rad/s
  f = @(q, k, m) (4*k/m)^0.5 * sin(pi*q/2) * 98.2291 / (2*pi); % in Hz
  
  %% Plot in frequency space
  
  k = 0.16498; % eV/A^2
  m1 = 40;
  m2 = 120;
  
  pcolor(ha(2*i_T_list-1), f(q_in, k, m1), f(q_out, k, m1), R_q);
  plot(  ha(2*i_T_list-1), [0 1.5], [0 1.5], '-', 'Color', 0.8*[1 1 1]);
  axis(  ha(2*i_T_list-1), [0 1.5 0 2.0]);
  caxis( ha(2*i_T_list-1), [cmin cmax]);
  ylabel(ha(2*i_T_list-1), 'Reflected {\it{ f }}'', THz');
  xlabel(ha(2*i_T_list-1), 'Initial $f_0$, THz', 'interpreter', 'latex');
  
  pcolor(ha(2*i_T_list), f(q_in, k, m1), f(q_out, k, m2), T_q);
  plot(  ha(2*i_T_list), [0 1.5], [0 1.5], '-', 'Color', 0.8*[1 1 1]);
  axis(  ha(2*i_T_list), [0 1.5 0 2.0]);
  caxis( ha(2*i_T_list), [cmin cmax]);
  ylabel(ha(2*i_T_list), 'Transmitted {\it{ f }}'', THz');
  xlabel(ha(2*i_T_list), 'Initial $f_0$, THz', 'interpreter', 'latex');
  
end

%% Label and print

cd(current_dir);

% Put a colorbar in one of the plots
axes(ha(1));
hcbar = colorbar('north');
% Push it around a bit
cpos = get(hcbar, 'Position');
cpos(2) = 1.07*cpos(2);
cpos(3) = 0.96*cpos(3);
cpos(4) = 0.4*cpos(4);
set(hcbar, 'Position', cpos);
% Customize
set(hcbar, 'XTick', [0, 0.01, 0.02, 0.025]);
set(hcbar, 'FontSize', 9);
xlabel(hcbar, 'Reflectivity/transmissivity, arb. units');

% Flip y-axis labels to the right
for ii = 2 : 2 : 2*length(T_list)
  set(ha(ii), 'YAxisLocation', 'right');
end

% Text annotations
text(0.8, 0.6, '${\beta} (f_0, f'')$', 'interpreter', 'latex');
text(2.5, 0.6, '${\alpha} (f_0, f'')$', 'interpreter', 'latex');
annotation('textarrow', [0.81, 0.86], [0.80, 0.75], 'string', '$f_0 = f''$', 'interpreter', 'latex');

print(gcf, '-dpng', '-r600', 'validation_spectra.png');