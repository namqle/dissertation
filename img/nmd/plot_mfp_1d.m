clear variables;
close all;

data_dir = '/Applications/Computation/nqlmd/research/';

%% plot settings

X = 3;
Y = 3;

% Plots
set(0, 'DefaultLineLineWidth', 0.5);
set(0, 'DefaultLineMarkerSize', 6);

% Figures
set(0, 'DefaultFigurePaperSize', [X Y]);
set(0, 'DefaultFigurePaperPositionMode', 'manual');
set(0, 'DefaultFigurePaperPosition', [ 0 0 X Y ] );
set(0, 'DefaultFigurePosition', [ 0 0 X*72 Y*72 ] );

% Comment here to switch on/off Times font
set(0, 'DefaultTextFontname', 'Times New Roman', ...
       'DefaultTextFontsize', 12, ...
       'DefaultAxesFontname', 'Times New Roman', ...
       'DefaultAxesFontsize', 12 );

%% Loop over masses

for mass = 40 : 80 : 120
  
%   figure;
  
  %% Loop over temperatures
  
  T_nom_list = 2 : 2 : 12;
  
  for i_T_nom = 1 : length(T_nom_list)
    
    T_nom = T_nom_list(i_T_nom);
    
    %% load
    
    load(sprintf('%s/1d_tau/m%d/T%02d/tau.mat', data_dir, mass, T_nom));
    
    %% calculate group velocities
    
    a = 5.313 + 4.792e-3*T_nom - 7.171e-5*T_nom^2 + 6.101e-6*T_nom^3;
    v = 2*pi*freq0(end)*a/2 * real(sqrt(1 - (freq0/freq0(end)).^2));
    
    %% add to a plot comparing all temperatures
    
    hold on;
    
    if mass == 40
      color = [i_T_nom-1, 0, length(T_nom_list)-i_T_nom] / (length(T_nom_list)-1);
      plot(freq0, v./(2*gamma) / 10, '.', 'color', color);
    elseif mass == 120
      color = [i_T_nom-1, 0.5*(length(T_nom_list)-1), length(T_nom_list)-i_T_nom] / (length(T_nom_list)-1);
      plot(freq0, v./(2*gamma) / 10, '.', 'color', color);
    end
    
    xlabel('{\it{f}}, THz');
    ylabel('{\it{\Lambda}}, nm');
    set(gca, 'xscale', 'log');
    set(gca, 'yscale', 'log');
    axis([0.1 3 0.1 1000]);
    
  end
  
end

%% Label and print

set(gca, 'XTick', [0.1, 1, 3]);

text(0.30, 300, '2 K');
text(0.12, 4, '12 K');

text(1.2, 30, '40 amu');
text(0.35, 0.2, '120 amu');

print(gcf, '-dpdf', 'mfp_1d.pdf');