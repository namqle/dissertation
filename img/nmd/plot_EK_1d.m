clear variables;
close all;

%% load

load('/Applications/Computation/nqlmd/research/1d_tau/m40/T08/tau.mat');

%% plot settings

X = 3.2;
Y = 3;

% Plots
set(0, 'DefaultLineLineWidth', 0.5);
set(0, 'DefaultLineMarkerSize', 3);

% Figures
set(0, 'DefaultFigurePaperSize', [X Y]);
set(0, 'DefaultFigurePaperPositionMode', 'manual');
set(0, 'DefaultFigurePaperPosition', [ 0 0 X Y ] );
set(0, 'DefaultFigurePosition', [ 0 0 X*72 Y*72 ] );

set(0, 'DefaultTextFontname', 'Times New Roman', ...
       'DefaultTextFontsize', 12, ...
       'DefaultAxesFontname', 'Times New Roman', ...
       'DefaultAxesFontsize', 12 );

%% pick out two modes

figure(1); clf;
ha = tight_subplot(1, 1, 0.01, [0.16, 0.04], [0.22, 0.03]);

for mode = 1 : 2
  
  modes = [50, 200];
  imode = modes(mode);
  
  if mode == 1
    color = [0, 1, 1];
  else
    color = [1, 0, 1];
  end
  
  %% Plot FFT[Qdot(t)^2]
  
  hold on;
  
  plot(GULP_freqs(imode)*[1 1], [1e-20 1e20], '-', 'color', 0.6*[1,1,1]);
  plot(freq, 10.^mean_log_data(imode,:), '.', 'color', color);
  plot(freq, 10.^(log_lorentzian([freq0(imode),gamma(imode)*gamma_scale,A(imode)],freq)), '-', 'color', 0.5*color);
  
  xlabel('{\it{f}}, THz');
  ylabel('{\it{E}}^{ K}_{{\it{q}},\nu}( {\it{f}} ) / ({\it{k}}_B{\it{T}} / 2),  THz^{-1}');
  set(gca, 'yscale', 'log');
  axis([0, 2, 1e-2, 1e2]);
  
end
  
%% Save as pdf

text(1.75, 50, '(a)');
set(gca, 'ticklength', [0.02, 0.06]);

set(ha, 'xticklabel', 0 : 0.5 : 2);
set(ha, 'yticklabel', logspace(-2, 2, 5));

print(figure(1), '-dpdf', 'EK_1d.pdf');