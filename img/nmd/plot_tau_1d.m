clear variables;
close all;

data_dir = '/Applications/Computation/nqlmd/research/1d_tau/';

%% plot settings

X = 3;
Y = 3;

% Plots
set(0, 'DefaultLineLineWidth', 0.5);
set(0, 'DefaultLineMarkerSize', 3);

% Figures
set(0, 'DefaultFigurePaperSize', [X Y]);
set(0, 'DefaultFigurePaperPositionMode', 'manual');
set(0, 'DefaultFigurePaperPosition', [ 0 0 X Y ] );
set(0, 'DefaultFigurePosition', [ 0 0 X*72 Y*72 ] );

% Comment here to switch on/off Times font
set(0, 'DefaultTextFontname', 'Times New Roman', ...
       'DefaultTextFontsize', 12, ...
       'DefaultAxesFontname', 'Times New Roman', ...
       'DefaultAxesFontsize', 12 );

%% Loop over masses

for mass = 40 : 80 : 120
  
  figure; hold on;
  
  %% Loop over temperatures
  
  % plot limit of tau = period = 1/f
  hp = plot(logspace(-2,1,5), 1./logspace(-2,1,5), '-', 'color', 0.5*[1,1,1]);
  
  T_nom_list = 2 : 2 : 12;
  for i_T_nom = 1 : length(T_nom_list)
    
    T_nom = T_nom_list(i_T_nom);
    
    %% load
    
    load(sprintf('%s/m%d/T%02d/tau.mat', data_dir, mass, T_nom));
    
    %% add to a plot comparing all temperatures
    
    if mass == 40
      color = [i_T_nom-1, 0, length(T_nom_list)-i_T_nom] / (length(T_nom_list)-1);
    elseif mass == 120
      color = [i_T_nom-1, 0.5*(length(T_nom_list)-1), length(T_nom_list)-i_T_nom] / (length(T_nom_list)-1);
    end
    
    plot(freq0, 1./(2*gamma), '.', 'color', color);
    
  end
  
  %% Label and print
  
  xlabel('{\it{f}}, THz');
  ylabel('{\it{\tau}}, ps');
  set(gca, 'xscale', 'log');
  set(gca, 'yscale', 'log');
  axis([0.01 3 0.1 1e3]);
  
  set(gca, 'XTick', [0.01, 0.1, 1, 3]);
  
  legend(hp, '{\it{\tau}} = {\it{f}} ^{-1}', 'location', 'sw');
  
  text(0.8, 30, '2 K');
  text(0.05, 4, '12 K');
  
  print(gcf, '-dpdf', sprintf('tau_1d_m%d.pdf', mass));
  
end