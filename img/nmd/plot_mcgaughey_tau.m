clear variables;
close all;

%% load

load('/Applications/Computation/nqlmd/research/3d_tau/m40/10x10x10_McGaughey2014/tau.mat');

%% plot settings

X = 3.2;
Y = 3;

% Plots
set(0, 'DefaultLineLineWidth', 0.5);
set(0, 'DefaultLineMarkerSize', 6);

% Figures
set(0, 'DefaultFigurePaperSize', [X Y]);
set(0, 'DefaultFigurePaperPositionMode', 'manual');
set(0, 'DefaultFigurePaperPosition', [ 0 0 X Y ] );
set(0, 'DefaultFigurePosition', [ 0 0 X*72 Y*72 ] );

set(0, 'DefaultTextFontname', 'Times New Roman', ...
       'DefaultTextFontsize', 12, ...
       'DefaultAxesFontname', 'Times New Roman', ...
       'DefaultAxesFontsize', 12 );

%% to compare with McGaughey2014

figure(1); clf;
ha = tight_subplot(1, 1, 0.01, [0.16, 0.04], [0.2, 0.03]);

tstar = 1/0.59008; % ps [this is sqrt((m*sig^2) / eps)]

% for imode = 1 : size(kpoints,1)
%   plot(2*pi*[freq0(imode), GULP_freqs(imode)] * tstar, 1./(2*gamma(imode))*[1,1] / tstar, '-', 'color', 0.7*[1,1,1]);
% end

plot(ha, 2*pi*freq0 * tstar, 1./(2*gamma) / tstar, 'b.');

xlabel('$\omega_{\mathbf{q},\nu} \, \sqrt{m \sigma^2 / \epsilon \;}$', 'interpreter', 'latex');
ylabel('$\tau_{\mathbf{q},\nu} \, / \sqrt{m \sigma^2 / \epsilon \;}$', 'interpreter', 'latex');

set(ha, 'xscale', 'log');
set(ha, 'yscale', 'log');

axis([2 30 0.2 100]);

set(ha, 'xticklabel', ' ');
text(0.97*2, 0.14, '2');
text(0.91*10, 0.14, '10');
text(0.91*30, 0.14, '30');
set(ha, 'ticklength', [0.02, 0.06]);

text(21, 60, '(b)');

print(gcf, '-dpdf', 'mcgaughey_tau.pdf');