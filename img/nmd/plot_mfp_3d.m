clear variables;
close all;

datadir = '/Applications/Computation/nqlmd/research/';

%% plot settings

X = 3;
Y = 3;

% Plots
set(0, 'DefaultLineLineWidth', 0.5);
set(0, 'DefaultLineMarkerSize', 12);

% Figures
set(0, 'DefaultFigurePaperSize', [X Y]);
set(0, 'DefaultFigurePaperPositionMode', 'manual');
set(0, 'DefaultFigurePaperPosition', [ 0 0 X Y ] );
set(0, 'DefaultFigurePosition', [ 0 0 X*72 Y*72 ] );

% Comment here to switch on/off Times font
set(0, 'DefaultTextFontname', 'Times New Roman', ...
       'DefaultTextFontsize', 12, ...
       'DefaultAxesFontname', 'Times New Roman', ...
       'DefaultAxesFontsize', 12 );

%% Loop over masses

for mass = 40 : 80 : 120
  
  figure;
  
  %% Loop over temperatures
  
  T_nom_list = 5 : 20 : 45;
  
  for i_T_nom = 1 : length(T_nom_list)
    
    T_nom = T_nom_list(i_T_nom);
    
    %% load
    
    load(sprintf('%s/3d_tau/m%d/T%02d/tau.mat', datadir, mass, T_nom));
    
    %% identify modes along high-symmetry directions
    
    % [100]
    mask_100 = kpoints(:,1)==0 & kpoints(:,3)==0;
    % [110]
    mask_110 = abs(kpoints(:,2)-kpoints(:,1))<1e-3 & kpoints(:,3)<1e-3;
    mask_XK = (abs(kpoints(:,2)-max(kpoints(:,2)))<1e-3) & (abs(kpoints(:,3)-kpoints(:,1))<1e-3);
    % [111]
    mask_111 = abs(kpoints(:,2)-kpoints(:,1))<1e-3 & abs(kpoints(:,3)-kpoints(:,2))<1e-3;
    
    %% calculate group velocities
    
    a = 5.313 + 4.792e-3*T_nom - 7.171e-5*T_nom^2 + 6.101e-6*T_nom^3;
    
    k_100 = pi/a * kpoints(mask_100,2) ./ max(kpoints(mask_100,2));
    v_100 = 2*pi * freq0(mask_100)*a ./ (2 * tan(k_100*a/2));
    
    k_111 = pi/a * kpoints(mask_111,1) ./ max(kpoints(mask_111,1));
    v_111 = 2*pi * freq0(mask_111)*a ./ (2 * tan(k_111*a/2));
    
    %% add to a plot comparing all temperatures
    
    hold on;
    
    if mass == 40
      color = [i_T_nom-1, 0, length(T_nom_list)-i_T_nom] / (length(T_nom_list)-1);
      plot(freq0(mask_100), v_100./(2*gamma(mask_100)) / 10, '.', 'color', color);
      plot(freq0(mask_111), v_111./(2*gamma(mask_111)) / 10, '.', 'color', color);
    elseif mass == 120
      color = [i_T_nom-1, 0.5*(length(T_nom_list)-1), length(T_nom_list)-i_T_nom] / (length(T_nom_list)-1);
      plot(freq0(mask_100), v_100./(2*gamma(mask_100)) / 10, '.', 'color', color);
      plot(freq0(mask_111), v_111./(2*gamma(mask_111)) / 10, '.', 'color', color);
    end
    
    xlabel('{\it{f}}, THz');
    ylabel('{\it{\Lambda}}, nm');
    set(gca, 'xscale', 'log');
    set(gca, 'yscale', 'log');
    axis([0.1 3 0.1 10000]);
    
  end
  
  %% Label and print
  
  if mass == 40
    text(0.15, 1e3, '5 K');
    text(0.11, 1.5e2, '25 K');
    text(0.11, 2e1, '45 K');
  elseif mass == 120
    text(1.3, 5, '5 K');
    text(1.3, 1, '25 K');
    text(1.2, 0.2, '45 K');
  end
  
  set(gca, 'XTick', [0.1, 1, 3]);
  set(gca, 'YTick', [1e-1, 1e0, 1e1, 1e2, 1e3, 1e4]);
%   set(gca, 'YTickLabel', {'0.1', '1', '10', '100', '1000', '10000'});
  
  print(gcf, '-dpdf', sprintf('mfp_3d_m%d.pdf', mass));
  
end

%% Label and print

% text(0.30, 300, '2 K');
% text(0.12, 4, '12 K');
% 
% text(1.2, 30, '40 amu');
% text(0.35, 0.2, '120 amu');
% 
% print(gcf, '-dpdf', 'mfp_3d.pdf');