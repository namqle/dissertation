clear variables;
close all;

%% Inputs

%% Plot settings

X = 6.5;
Y = 4;

% Axes
set(0, 'DefaultAxesBox', 'on');
set(0, 'DefaultAxesLineWidth', 0.5);
set(0, 'DefaultAxesXMinorTick', 'on');
set(0, 'DefaultAxesYMinorTick', 'on');
set(0, 'DefaultAxesTickDir', 'in');

% Plots
set(0, 'DefaultLineLineWidth', 1.5);
set(0, 'DefaultLineMarkerSize', 4);

% Figures
set(0, 'DefaultFigurePaperSize', [X Y]);
set(0, 'DefaultFigurePaperPositionMode', 'manual');
set(0, 'DefaultFigurePaperPosition', [ 0 0 X Y ] );
set(0, 'DefaultFigurePosition', [ 0 0 X*72 Y*72 ] );

% Comment here to switch on/off Times font
set(0, 'DefaultTextFontname', 'Times New Roman', ...
       'DefaultTextFontsize', 12, ...
       'DefaultAxesFontname', 'Times New Roman', ...
       'DefaultAxesFontsize', 12 );

%% Read GULP calculations

raw = textscan(fopen('gulp/ar_100.disp','r'), '%f %f', 'CommentStyle', '#');
fgulp_100 = raw{2} * 3e10/1e12;

raw = textscan(fopen('gulp/ar_110.disp','r'), '%f %f', 'CommentStyle', '#');
fgulp_110 = raw{2} * 3e10/1e12;

raw = textscan(fopen('gulp/ar_111.disp','r'), '%f %f', 'CommentStyle', '#');
fgulp_111 = raw{2} * 3e10/1e12;

%% Read experimental data from Barker1970

raw = textscan(fopen('Barker1970/100.txt','r'), '%f %f');
qexp_100 = raw{1};
fexp_100 = raw{2};

raw = textscan(fopen('Barker1970/110.txt','r'), '%f %f');
qexp_110 = raw{1};
fexp_110 = raw{2};

raw = textscan(fopen('Barker1970/111.txt','r'), '%f %f');
qexp_111 = raw{1};
fexp_111 = raw{2};

%% Plot

figure(1);

% Plot 100 (Gamma -> X)


% Plot 110 (Gamma -> K -> X)

% Plot 111 (Gamma -> L)