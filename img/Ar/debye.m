clear variables;
close all;

%% plot settings

X = 3.2;
Y = 3;

% Plots
set(0, 'DefaultLineLineWidth', 1.5);
set(0, 'DefaultLineMarkerSize', 10);

% Figures
set(0, 'DefaultFigurePaperSize', [X Y]);
set(0, 'DefaultFigurePaperPositionMode', 'manual');
set(0, 'DefaultFigurePaperPosition', [ 0 0 X Y ] );
set(0, 'DefaultFigurePosition', [ 0 0 X*72 Y*72 ] );

set(0, 'DefaultTextFontname', 'Times New Roman', ...
       'DefaultTextFontsize', 12, ...
       'DefaultAxesFontname', 'Times New Roman', ...
       'DefaultAxesFontsize', 12 );

%% calculations

% constants
a = 5.313e-10; % lattice parameter

% maximum frequency
w_max = 2*pi*2.01e12; % rad/s

% frequency variable
ww_D = linspace(0, 1.8*w_max, 1000);

% sound speeds
vL = w_max*a/4;
vT = vL / sqrt(2);

% Debye frequency
wL_D_max = (6*pi^2 * 4/a^3)^(1/3) * vL;
wT_D_max = (6*pi^2 * 4/a^3)^(1/3) * vT;

% Debye DOS

DL_D = zeros(size(ww_D));
DT_D = zeros(size(ww_D));

DL_D(ww_D<wL_D_max) = ww_D(ww_D<wL_D_max).^2 / (2*pi^2*vL^3);
DT_D(ww_D<wT_D_max) = ww_D(ww_D<wT_D_max).^2 / (2*pi^2*vT^3);

D_D = 2*DT_D + DL_D;

%% plot Debye DOS

w_scale = 2*pi*1e12;
D_scale = 1e16;

figure(1); hold on;

D_D = D_D / trapz(ww_D/(2*pi), D_D) * 12/a^3;

plot(D_D/D_scale, ww_D/w_scale, 'k--');


%% plot GULP DOS

raw = textscan(fopen('gulp/ar_100.dens', 'r'), '%f %f', 'CommentStyle', '#');

ww_G = raw{1} * 2*pi*3e10; % rad/s
D_G = raw{2};
clear raw;

D_G = D_G / trapz(ww_G/(2*pi), D_G) * 12/a^3;

plot(D_G/D_scale, ww_G/w_scale, 'b-');

%% Save DOS plot

axis([0, 15, 0, 4]);

ylabel('{\it{f}},  THz');
xlabel('{\it{D}}( {\it{f}} ),  10^{16} states Hz^{-1} m^{-3}');

text(0.9, 3.6, '(b)');
text(10, 0.9, sprintf('Lattice\ndynamics,\nfull BZ'));
text(4, 2.5, 'Debye');

print(gcf, '-dpdf', 'debye_D.pdf');

%% plot Debye dispersions

qq = linspace(0, 2*pi/a, 100);

figure(2); hold on;
q_scale = 2*pi/a;

wL_D = zeros(size(qq));
wT_D = zeros(size(qq));

wL_D(wL_D<wL_D_max) = vL * qq(wL_D<wL_D_max);
wT_D(wT_D<wT_D_max) = vT * qq(wT_D<wT_D_max);

plot(qq/q_scale, wL_D/w_scale, 'k--');
plot(qq/q_scale, wT_D/w_scale, 'k--');

%% plot lattice dynamics dispersions

wL = w_max * sin(qq*a/4);
wT = wL / sqrt(2);

plot(qq/q_scale, wL/w_scale, 'b-');
plot(qq/q_scale, wT/w_scale, 'b-');


%% Save dispersions plot

axis([0, 1, 0, 4]);

xlabel('{\it{q}} / {\it{q}}_{max}');
ylabel('{\it{\omega}}_{q,\nu} / (2\pi),  THz');

text(0.06, 3.6, '(a)');
text(0.65, 0.7, sprintf('Lattice\ndynamics,\n[001]'));
text(0.6, 2.7, 'Debye');

h1 = text(0.10, 0.65, 'longitudinal', 'fontsize', 9, 'color', 0.4*[1,1,1]);
h2 = text(0.17, 0.2, '2 \times transverse', 'fontsize', 9, 'color', 0.4*[1,1,1]);

set(h1, 'rotation', 36);
set(h2, 'rotation', 26);

print(gcf, '-dpdf', 'debye_wq.pdf');

