clear variables;
close all;

%% plot settings

X = 6.5;
Y = 3;

% Plots
set(0, 'DefaultLineLineWidth', 1.5);
set(0, 'DefaultLineMarkerSize', 10);

% Figures
set(0, 'DefaultFigurePaperSize', [X Y]);
set(0, 'DefaultFigurePaperPositionMode', 'manual');
set(0, 'DefaultFigurePaperPosition', [ 0 0 X Y ] );
set(0, 'DefaultFigurePosition', [ 0 0 X*72 Y*72 ] );

% Comment here to switch on/off Times font
set(0, 'DefaultTextFontname', 'Times New Roman', ...
       'DefaultTextFontsize', 12, ...
       'DefaultAxesFontname', 'Times New Roman', ...
       'DefaultAxesFontsize', 12 );

%% Plot lattice dynamics first

LD_color = 0.4*[1,1,1];

hf2 = figure(2);
ha2 = tight_subplot(1, 3, [0.1,0.01], [0.07,0.03], [0.12,0.02]);
for ii = 1 : 3; hold(ha2(ii), 'on'); end

% 100
raw = textscan(fopen('gulp/ar_100.disp', 'r'), '%f %f', 'commentstyle', '#');
f = raw{2} * 3e10/1e12;
q = linspace(0, 1, length(f)/3);
hp_ld = plot(ha2(1), q, f(1:3:end), '-', 'linewidth', 1, 'color', LD_color);
plot(ha2(1), q, f(3:3:end), '-', 'linewidth', 1, 'color', LD_color);

% 110
raw = textscan(fopen('gulp/ar_110.disp', 'r'), '%f %f', 'commentstyle', '#');
f = raw{2} * 3e10/1e12;
q = linspace(0, 1, length(f)/3);
plot(ha2(2), q, f(1:3:end), '-', 'linewidth', 1, 'color', LD_color);
plot(ha2(2), q, f(2:3:end), '-', 'linewidth', 1, 'color', LD_color);
plot(ha2(2), q, f(3:3:end), '-', 'linewidth', 1, 'color', LD_color);

% 111
raw = textscan(fopen('gulp/ar_111.disp', 'r'), '%f %f', 'commentstyle', '#');
f = raw{2} * 3e10/1e12;
q = linspace(0, 1, length(f)/3);
plot(ha2(3), q, f(1:3:end), '-', 'linewidth', 1, 'color', LD_color);
plot(ha2(3), q, f(3:3:end), '-', 'linewidth', 1, 'color', LD_color);

fclose('all');

%% Loop over temperatures

T_nom_list = 5 : 10 : 45;

for i_T_nom = 1 : length(T_nom_list)

  T_nom = T_nom_list(i_T_nom);
  
  %a = 5.313 + 1.813e-3*(0.5*T_nom) + 4.792e-6*(0.5*T_nom)^2 + 1.394e-8*(0.5*T_nom)^3;
  a = 5.313;
  kmax = 2*pi/a;

  %% load

  load(sprintf('/Applications/Computation/nqlmd/research/3d_tau/m40/T%02d/tau.mat', T_nom));

  %% masks
  
  % [100]
  mask_100 = kpoints(:,1)==0 & kpoints(:,3)==0;
  % [110]
  mask_110 = abs(kpoints(:,2)-kpoints(:,1))<1e-3 & kpoints(:,3)<1e-3;
  mask_XK = (abs(kpoints(:,2)-max(kpoints(:,2)))<1e-3) & (abs(kpoints(:,3)-kpoints(:,1))<1e-3);
  % [111]
  mask_111 = abs(kpoints(:,2)-kpoints(:,1))<1e-3 & abs(kpoints(:,3)-kpoints(:,2))<1e-3;

  %% add plots at this temperature

  color = [i_T_nom-1, 0, length(T_nom_list)-i_T_nom] / (length(T_nom_list)-1);

  if i_T_nom == 1
    hp_nmd_cold = plot(ha2(1), kpoints(mask_100,2)/kmax, freq0(mask_100), '.', 'color', color);
  elseif i_T_nom == length(T_nom_list)
    hp_nmd_hot = plot(ha2(1), kpoints(mask_100,2)/kmax, freq0(mask_100), '.', 'color', color);
  else
    plot(ha2(1), kpoints(mask_100,2)/kmax, freq0(mask_100), '.', 'color', color);
  end
  axis(ha2(1), [0 1 0 2.5]);
  set(ha2(1), 'xtick', [0 1], 'xticklabel', {'0', ' X'});
  set(ha2(1), 'ytick', 0 : 0.5 : 2.5, 'yticklabel', {'0', '0.5', '1.0', '1.5', '2.0', '2.5'});
  ylabel(ha2(1), '{\it{\omega}} / (2\pi), THz');
  
  plot(ha2(2), kpoints(mask_110,2)/kmax, freq0(mask_110), '.', 'color', color);
  plot(ha2(2), (1-kpoints(mask_XK,1)/kmax), freq0(mask_XK), '.', 'color', color);
  axis(ha2(2), [0 1 0 2.5]);
  set(ha2(2), 'xtick', [0 3/4 1], 'xticklabel', {'', 'K', ''});
  set(ha2(2), 'ytick', 0 : 0.5 : 2.5, 'yticklabel', '');
  set(ha2(2), 'xdir', 'reverse');
  
  plot(ha2(3), kpoints(mask_111,2)/(kmax/2), freq0(mask_111), '.', 'color', color);
  axis(ha2(3), [0 1 0 2.5]);
  set(ha2(3), 'xtick', [0 1], 'xticklabel', {'0 ', 'L'});
  set(ha2(3), 'ytick', 0 : 0.5 : 2.5, 'yticklabel', '');

end

%% Plot experimental data on top

raw = textscan(fopen('Barker1970/100.txt','r'), '%f %f');
qexp_100 = raw{1};
fexp_100 = raw{2};

raw = textscan(fopen('Barker1970/110.txt','r'), '%f %f');
qexp_110 = raw{1};
fexp_110 = raw{2};

raw = textscan(fopen('Barker1970/111.txt','r'), '%f %f');
qexp_111 = raw{1};
fexp_111 = raw{2};

hp_exp = plot(ha2(1),   qexp_100(1:3:end), fexp_100(1:3:end), 'kx');
plot(ha2(2),   qexp_110(1:3:end), fexp_110(1:3:end), 'kx');
plot(ha2(3), 2*qexp_111(1:3:end), fexp_111(1:3:end), 'kx');

hl = legend([hp_exp, hp_ld, hp_nmd_cold, hp_nmd_hot], ...
  'Expt. (4 K)', 'LD (0 K)', 'MD (5 K)',  'MD (45 K)', ...
  'location', 'nw');
legend(ha2(1), 'boxoff');
set(hl, 'fontsize', 10);


%% Save plot

print(hf2, '-dpdf', 'nmd_vs_exp.pdf');

fclose('all');
