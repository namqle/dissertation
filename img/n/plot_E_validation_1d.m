clear variables;
close all;

%% Settings

img_dir = pwd;
data_dir = '/Applications/Computation/nqlmd/research/1d_nemd_wavelet/old';

N = 11232;
qpoints = 100;

kB = 0.83148; % in amu A^2 / (ps^2 K)
aap2eV = 1.036382e-4;

%% Manual plot settings
X = 3.2;
Y = 2;

% Axes
set(0, 'DefaultAxesBox', 'on');
set(0, 'DefaultAxesLineWidth', 0.5);
set(0, 'DefaultAxesXMinorTick', 'on');
set(0, 'DefaultAxesYMinorTick', 'on');
set(0, 'DefaultAxesTickDir', 'out');

% Plots
set(0, 'DefaultLineLineWidth', 0.5);
set(0, 'DefaultLineMarkerSize', 4);

% Figures
set(0, 'DefaultFigurePaperSize', [X Y]);
set(0, 'DefaultFigurePaperPositionMode', 'manual');
set(0, 'DefaultFigurePaperPosition', [ 0 0 X Y ] );
set(0, 'DefaultFigurePosition', [ 0 0 X*72 Y*72 ] );

% Comment here to switch on/off Times font
set(0, 'DefaultTextFontname', 'Times New Roman', ...
       'DefaultTextFontsize', 12, ...
       'DefaultAxesFontname', 'Times New Roman', ...
       'DefaultAxesFontsize', 12 );

%% One type of system at a time

jobset_list = 1;
jobset_types = {'lj'};
for ij = 1 : length(jobset_list)
  
  %% One temperature at a time
  
  T_list = 2;
  for iT = 1 : length(T_list)
    
    
    %% Average over independent runs
    rho_energy_mean = zeros( N, qpoints );
    for irun = 1 : 5

      % Load the archive
      load(sprintf('%s/T%02d_equilibrium/jobset%02d-r%02d-%s/archive.mat', data_dir, T_list(iT), jobset_list(ij), irun, jobset_types{ij}));
      
      % Update the running average
      rho_energy_mean = rho_energy_mean + (rho_energy - rho_energy_mean)/irun; % in eV
      
    end
    
    %% Make a plot
    
    figure;
    ha = tight_subplot(1, 1, 0, [0.28, 0.05], [0.22, 0.17]);
    
    % signal to plot
    z = 0.5*rho_energy_mean/aap2eV/kB * 2*pi*(q(end)-q(1))*qm;
    
    % colormap
    h1 = pcolor(ha, (d-N*a/4)/1e4, 2*pi*q*qm / (2*pi/a), z');
    set(h1, 'facecolor', 'interp', 'edgecolor', 'interp');
    axis([-1.5, 1.5, 0.0, 0.7]);
    set(gca, 'xtick', -1.5 : 0.75 : 1.5, 'ytick', 0.0 : 0.35 : 0.7);
    
    % colorbar
    hcbar = colorbar;
    caxis(T_list(iT) * [0.7, 1.3]);
    cpos = get(hcbar, 'Position');
    cpos(1) = 1.3*cpos(1);
    cpos(3) = 0.7*cpos(3);
    set(hcbar, 'position', cpos);
    xlabel(hcbar, '       K');
    
    if iT == length(T_list)
      xlabel('{\it{z}}, {\mu}m');
    end
    if strcmp(jobset_types{ij}, 'lj')
      ylabel('{\it{q}} / {\it{q}}_{max}');
    end
    
    text(14, 0.6, '{\bf{(b)}}', 'color', 'k');
    
    print(gcf, '-dpng', '-r600', sprintf('E_1d_T%02d_%s.png', T_list(iT), jobset_types{ij}));
    
    %% For fun, plot the deviation from the average value AT THAT LOCATION
%     
%     figure;
%     
%     z = z - repmat(mean(z,2), [1, size(rho_energy,2)]);
%     h1 = pcolor((d-N*a/4)/1e4, 2*pi*q*qm / (2*pi/a), z');
%     set(h1, 'facecolor', 'interp', 'edgecolor', 'interp');
%     hcbar = colorbar;
%     
%     cmax = max(max(abs(z)));
%     caxis(cmax*[-1,1]);
%     axis([-1.5, 1.5, 0, 0.7]);
%     set(gca, 'xtick', -1.5 : 1.5 : 1.5, 'ytick', 0 : 0.35 : 0.7);
%     
%     colormap( pmkmp(192,'edge') );
%     
%     xlabel('{\it{z}}, {\mu}m');
%     ylabel('{\it{q}} / {\it{q}}_{max}');
    
  end
  
end