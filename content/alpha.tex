\chapter{Effects of Anharmonicity on Phonon Transmission}
\label{sec:alpha}

%\begin{quote}
%  We must look for consistency. Where there is a want of it we must suspect deception.
%
%  \raggedleft{--- Sir Arthur Conan Doyle, ``The Problem of Thor Bridge,'' 1922~\cite{Doyle1981}}
%\end{quote}

Chapter~\ref{sec:h} presented the effect of anharmonicity on the macroscopic heat transfer at an interface, as captured in the thermal conductance $h(T)$. The macroscopic effects observed in that chapter must be explained by some microscopic phenomena. The theory established in Section~\ref{sec:methods_interfaces} provides a list of phononic quantities that could be responsible, of which the two likely candidates are the transmissivity $\alpha_{\mathbf{q},\nu}$ and the distribution $n_{\mathbf{q},\nu}$. In this chapter, I present results assessing the effect of anharmonicity on $\alpha_{\mathbf{q},\nu}$, and in doing so I also consider the potential effect on $h(T)$. I calculate $\alpha_{\mathbf{q},\nu}$ using the wave packet method, described in Section~\ref{sec:alpha_packets}. I present the results in Section~\ref{sec:alpha_results}, and I discuss their implications in Section~\ref{sec:alpha_discussion}. The effect of anharmonicity is to enable transmission into modes of different frequency (inelastic processes), but the net transmissivity decreases, contrary to my expectation. Therefore, it appears that an increase in transmission is unlikely to be the sole mechanism by which anharmonicity increases $h(T)$.

\section{The wave packet method}
\label{sec:alpha_packets}

Wave packet simulations are the computational analogue of ``phonon-pulse experiments,'' which are described in Section III.C.1 of Ref.~\cite{Swartz1989}. Given a sample structure consisting of an interface between two phases, energy is deposited into a narrow band of normal modes in one of the materials and allowed to propagate and refract through the interface. The fraction of energy that successfully passes through the interface provides an aggregate transmissivity of the initial band of normal modes.

\subsection{Methodology}
\label{sec:alpha_packets_methodology}

By convention I orient the systems so that the wave packet originates in the ``left'' material and transmits into the ``right'' material. The wave packet simulations are similar in geometry to the NEMD simulations, except with different lengths of each material. In the 1D simulations, the domain consists of 2800 Ar atoms and 2000 heavy Ar atoms, for a total length of about 1.28~$\upmu$m. In the 3D simulations, the domain consists of $30 \times 30 \times 40$ unit cells of Ar (144\,000 atoms) abutting with $30 \times 30 \times 20$ unit cells of heavy Ar (72\,000 atoms). The large 3D domain is necessary for simulations of wave packets at non-normal incidence angles, as shown in Section~\ref{sec:alpha_results_3d}.

I essentially follow the method described by Schelling~\textit{et~al.}~\cite{Schelling2002} and those who later generalized the method to non-normal incidence~\cite{Kimmer2007,Aubry2008,Wei2012,Deng2014}. The process can be broken into three parts: (1) system initialization with energy concentrated about a single normal mode, (2) simulation of the partial reflection/transmission event, and (3) data collection.

\begin{enumerate}

  \item Each simulation begins with energy concentrated about a particular normal mode $(\mathbf{q}_0,\nu_0)$ in reciprocal space and about a spatial location $\mathbf{r}_0$ in the left material. To do so, atoms are given an initial displacement from their equilibrium positions $\textbf{r}_\text{eq}$, such that the position of the $j^\text{th}$ basis atom in the $l^\text{th}$ unit cell is
  \begin{equation}
    \label{eqn:alpha_packets_methodology_r}
    \mathbf{r}_{j,l}(t_\text{initial}) = \mathbf{r}_{j,l,\text{eq}} + \text{Re} \left\{ \frac{B}{\omega_{\mathbf{q}_0,\nu_0}} \, \mathbf{e}_{\mathbf{q}_0,\nu_0,j} \exp \left[ i \left( \mathbf{q}_0 \cdot \mathbf{r}_{j,l,\text{eq}} - \omega_{\mathbf{q}_0,\nu_0} t_\text{initial} \right) \right] \\
    \exp \left( - \frac{|\mathbf{r}_{j,l} - \mathbf{r}_0|^2}{\eta^2} \right) \right\},
  \end{equation}
  where the wave packet amplitude is $B \omega^{-1}$, and its characteristic width is $\eta$. See Section~\ref{sec:methods_normalmodes} for explanation of the other quantities. The initial velocities are therefore
  \begin{equation}
    \label{eqn:alpha_packets_methodology_v}
    \mathbf{v}_{j,l}(t_\text{initial}) = \frac{d \mathbf{r}_{j,l}(t_\text{initial})}{d t} = \text{Re} \left\{ -i \omega_{\mathbf{q}_0,\nu_0} \left[ \mathbf{r}_{j,l}(t_\text{initial}) - \mathbf{r}_{j,l,\text{eq}} \right] \right\}.
  \end{equation}

  \item The molecular dynamics simulation proceeds according to the rules of Section~\ref{sec:methods_atoms_md}. If the initial conditions were set correctly, the wave packet propagates naturally at its group velocity toward the interface and partially transmits. The simulation should end when the transmitted and reflected packets have completely left the interface.

  \item The energy on each side, and therefore the transmissivity, can be calculated directly from the atomic positions and velocities.
\end{enumerate}
Ideally, the initial packet is composed of a narrow band of modes, in which case we assign the resulting transmissivity to the central mode $(\mathbf{q}_0, \nu_0)$. The width of the initial packet in $q$-space therefore leads to an uncertainty in assigning $\alpha_{\mathbf{q}_0,\nu_0,1 \to 2}$.

I note that previous works have used a version of Eq.~\ref{eqn:alpha_packets_methodology_r} where the maximum displacement is not divided by the frequency; i.e.,\ having a prefactor of just $B$ instead of $B \omega^{-1}$. I use the latter because it ensures that packets of equal $B$ correspond to the same energy, and can therefore be associated with normal mode vibrations of the same temperature in a system at thermal equilibrium. This distinction is irrelevant in the harmonic (i.e.,\ small-amplitude) limit, as has been simulated in Refs.~\cite{Schelling2002,Kimmer2007,Aubry2008,Wei2012,Deng2014}. In this case, the transmission is linear; i.e.,\ it does not depend on amplitude. However, the distinction is important in this work, since I am interested in anharmonic processes that are inherently non-linear: inelastic transmission that depends on vibrational amplitude.

Therefore, I seek to associate with each value $B$ a (roughly) equivalent temperature $T_\text{equiv}$. From Eq.~\ref{eqn:alpha_packets_methodology_v}, the value of $B$ corresponds to the maximum velocity of any atom in the wave packet. A plane wave of that amplitude has a root-mean-square value of $B/\sqrt{2}$, which I equate to the root-mean square velocity of an atom in a system at thermal equilibrium at temperature $T$:

\begin{equation}
  \frac{B}{\sqrt{2}} \sim \sqrt{\frac{d k_\text{B} T}{m}}.
\end{equation}
Based on this relation, a reasonable definition of an equivalent temperature is

\begin{equation}
  \label{eqn:alpha_packets_methodology_Tequiv}
  T_\text{equiv} \equiv \frac{m B^2}{2 d k_\text{B}}.
\end{equation}
In interpreting the following results, I use Eq.~\ref{eqn:alpha_packets_methodology_Tequiv} to provide an approximate connection between wave packet amplitudes and the NEMD simulations of Chapter~\ref{sec:h}. 

\subsection{Verification}
\label{sec:alpha_packets_validation}

I verify the method by first performing simulations in one-dimensional systems with harmonic bonds only. In such systems, the transmissivity can be calculated analytically~\cite{Saltonstall2013}:

\begin{equation}
  \label{eqn:alpha_packets_validation_salt}
    \alpha_{1 \to 2}(\omega) = \frac{4 \Gamma_1 \Gamma_2}{{(\Gamma_1 + \Gamma_2)}^2 + \omega^4 {(m_1 - m_2)}^2},
\end{equation}
where

\begin{equation}
  \Gamma_i = 2 \omega \sqrt{m_i k - \frac{m_i^2 \omega^2}{4}},
\end{equation}
where $k$ is the harmonic force constant. In the following, I show that transmissivities calculated using the wave packet method are consistent with those calculated using Eq.~\ref{eqn:alpha_packets_validation_salt}.

The trajectories of two wave packet simulations in 1D, harmonic systems are shown in Fig.~\ref{fig:alpha_packets_validation_v}: (a) one starting in Ar with $q_0 = 0.3 \, q_\text{max}$ and (b) the second starting in heavy Ar with $q_0 = 0.6 \, q_\text{max}$, where $q_\text{max} = \frac{2 \pi}{a}$. The width of all packets is $\eta = 30a$, and the amplitudes in both simulations correspond to $T_\text{equiv} = 2$~K: (a) $B = 0.288$~\AA/ps and (b) $B = 0.166$~\AA/ps. Each plot provides five snapshots of the velocities $v_i$ of atoms $i$ within 250~nm of the interface at intervals of 25~ps. After 100~ps, the reflected and transmitted packets have completely left the interface, and the ratios of reflected and transmitted energy are calculated.

\begin{figure}[tbp]
  \begin{center}
    \includegraphics{img/alpha/validation_v_12}
    \includegraphics{img/alpha/validation_v_21}
    \caption[Snapshots of atomic velocities in two wave packet simulations with all harmonic forces.]{Snapshots of atomic velocities $v(z,t)$ in two wave packet simulations with all harmonic forces and the percentages of reflected and transmitted energy. (a) Ar to heavy Ar, $q_0 = 0.3 \, q_\text{max}$. (b) Heavy Ar to Ar, $q_0 = 0.6 \, q_\text{max}$. The initial amplitudes are given in the text.}
    \label{fig:alpha_packets_validation_v}
  \end{center}
\end{figure}

\begin{figure}[tbp]
  \begin{center}
    \includegraphics{img/alpha/validation_alpha}
    \caption[Transmissivity of normal modes as a function of their frequency in systems with harmonic forces throughout.]{Transmissivity of normal modes as a function of their frequency in systems with harmonic forces throughout. For clarity, the uncertainty is only shown for one point in each data series.}
    \label{fig:alpha_packets_validation}
  \end{center}
\end{figure}

To test the methodology, I ran 38 simulations (22 starting in Ar, 16 in heavy Ar) similar to those shown in Fig.~\ref{fig:alpha_packets_validation_v} at different wavenumbers and compiled the transmissivity values. The results are plotted as a function of normal mode frequency, $\alpha_{q,i \to j}(\omega_q)$, in Fig.~\ref{fig:alpha_packets_validation}, along with the prediction of Eq.~\ref{eqn:alpha_packets_validation_salt}.\footnote{Throughout this work, I follow the common convention of using angular frequency $\omega$ in developing theory but linear frequency $f$ in reporting actual values.} The dominant uncertainty arises from assigning the transmissivity to the nominal wavenumber $q$, despite the wavepacket containing components from a band of wavenumbers of characteristic width $2 \pi / \eta$. The corresponding frequency range is marked by error bars for two representative points. The data agree very well with Eq.~\ref{eqn:alpha_packets_validation_salt}, and I am satisfied that this implementation of the wave packet method produces correct results.

Finally, I also verify the assertion that only elastic (frequency-preserving) scattering is possible in a system with only harmonic forces. For each simulation from Ar to heavy Ar---i.e.,\ the red crosses in Fig.~\ref{fig:alpha_packets_validation}---I determine the spectra of energy transmitted, $\alpha(\omega_0,\omega')$, and reflected, $\beta(\omega_0,\omega')$, from an initial frequency $\omega_0$ into a final frequency $\omega'$. The initial frequency in each simulation is known from the initial wavenumber, and the spectrum of final frequencies is obtained by Fourier transform of the final velocities. Specifically, I use the final atomic velocities as a spatial signal $v_i(t_\text{final}) = v_\text{final}(z)$, which is related to the spectrum of kinetic energy in the final system, $E^\text{K}_{q_0}(q') = E^\text{K}(q_0,q')$, by a Fourier transform in $z$:

\begin{equation}
  \label{eqn:alpha_packets_validation_Eqq}
  E^\text{K}(q_0,q') = \left| \mathcal{F} \left\{ \sqrt{\frac{m(z)}{2}} \; v_\text{final}(z) \right\} \right|^2,
\end{equation}
where the atomic masses have also been converted to a spatial signal, $m_i = m(z)$. Parseval's theorem ensures that the total energy in the spectrum is equal to the total kinetic energy in the system:

\begin{equation}
  E^\text{K}(q_0) = \int E^\text{K}(q_0,q') \, dq' = \frac{1}{2} \sum_i^N m_i v_i^2.
\end{equation}
By including only atoms of one material, one can also obtain the spectra of only the energy that has been reflected back into material 1 or transmitted into material 2:

\begin{align}
  E^\text{K}_1(q_0,q') &= \left| \mathcal{F} \left\{ \sqrt{\frac{m(z<0)}{2}} \; v_\text{final}(z<0) \right\} \right|^2 \text{~and} \\
  E^\text{K}_2(q_0,q') &= \left| \mathcal{F} \left\{ \sqrt{\frac{m(z>0)}{2}} \; v_\text{final}(z>0) \right\} \right|^2.
\end{align}
In terms of these quantities, I define the reflectivity and transmissivity spectra as

\begin{equation}
  \beta(q_0,q')  = \frac{E^\text{K}_1(q_0,q')}{E^\text{K}(q_0)} \quad \text{and} \quad
  \alpha(q_0,q') = \frac{E^\text{K}_2(q_0,q')}{E^\text{K}(q_0)},
\end{equation}
respectively. To evaluate the extent of elastic and inelastic processes, these are readily converted to frequency space by using the dispersion relation, Eq.~\ref{eqn:methods_normalmodes_ld_dispersion}.

\begin{figure}[tbp]
  \begin{center}
    \includegraphics{img/alpha/validation_spectra}
    \caption[Reflectivity and transmissivity spectra of modes in a system with harmonic forces throughout.]{Reflectivity and transmissivity of modes from initial frequency $f_0$ into final frequency $f'$ in the Ar/heavy Ar system with harmonic forces throughout.}
    \label{fig:alpha_packets_validation_spectra}
  \end{center}
\end{figure}

Thus I plot $\beta(\omega_0,\omega')$ and $\alpha(\omega_0,\omega')$ in Fig.~\ref{fig:alpha_packets_validation_spectra}. The plots show that, in each simulation, all of the energy is reflected and transmitted into the same frequency $\omega'$ as the initial frequency $\omega_0$ (within the uncertainty due to the finite packet broadness). This confirms the expectation that only elastic transmission occurs in a harmonic system.


\section{Results: Phonon transmission spectra}
\label{sec:alpha_results}

To test the hypotheses that thermal conductance increases at high temperature due to additional transmission via inelastic scattering~\cite{Stoner1992,Stoner1993,Lyeo2006,Hohensee2015}, I now use this simulation scheme to calculate $\alpha(\omega_0,\omega')$ at interfaces with anharmonicity. In both one- and three-dimensional systems, I report the transmissivity spectra in systems with all harmonic bonds except between the two atoms (1D) or atomic planes (3D) immediately adjacent to the interface, which interact through the LJ potential. These systems correspond to the systems in which I presented NEMD simulations of conductance in case (c) of Section~\ref{sec:h_results}. Varying the amplitude of the wave packet provides insight into interfacial transmission in systems at different temperatures, as captured roughly by the equivalent temperature defined in Eq.~\ref{eqn:alpha_packets_methodology_Tequiv}.

\subsection{In one dimension}
\label{sec:alpha_results_1d}

From the arguments of Chapters~\ref{sec:intro} and~\ref{sec:methods}, one expects that adding anharmonicity to the forces at the interface should enable inelastic reflection and transmission processes. Therefore I perform simulations in the same 1D, harmonic systems as in Section~\ref{sec:alpha_packets_validation}, except that I use the LJ potential to describe the interactions between the two atoms at the interface. Figure~\ref{fig:alpha_results_1d_v} shows snapshots of $v_i(t)$ from two $q = 0.3 \, q_\text{max}$ packets originating in Ar with different amplitudes, corresponding to (a) $B = 0.288$~\AA/ps ($T_\text{equiv} = 2$~K) and (b) $B = 0.706$~\AA/ps (12~K). On each plot, the fraction of energy reflected/transmitted in each mode is noted next to the corresponding packet in the signal at $t = 100$~ps.

\begin{figure}[htbp]
  \begin{center}
    \includegraphics{img/alpha/v_1d_Ar_T02}
    \includegraphics{img/alpha/v_1d_Ar_T12}
    \caption[Snapshots of atomic velocities $v(z,t)$ in wave packet simulations with LJ forces at the interface.]{Snapshots of atomic velocities $v(z,t)$ in two wave packet simulations with LJ forces at the interface: (a) $T_\text{equiv} = 2$~K and (b) $T_\text{equiv} = 12$~K.}
    \label{fig:alpha_results_1d_v}
  \end{center}
\end{figure}

I make two observations. First, there is a new reflected packet that was not present in the all-harmonic system (Fig.~\ref{fig:alpha_packets_validation_v}a). The packet evidently has a slower group velocity, indicating that it has a higher frequency. This confirms that the anharmonic forces at the interface have enabled an inelastic process. Second, the reflectivity and transmissivity now change with amplitude, consistent with the fact that the inelastic process is non-linear. As expected, the rate of the inelastic process increases with amplitude. However, somewhat surprisingly, the total transmissivity decreases.

\begin{figure}[tbp]
  \begin{center}
    \includegraphics{img/alpha/alpha_1d_Ar_hAr.png}
    \caption{Energy reflection and transmission spectra from Ar to heavy Ar with LJ forces at the interface. (a) Reflection and (b) transmission spectra with amplitude corresponding to $T_\text{equiv} = 2$~K, (c,d) the same with $T_\text{equiv} = 12$~K, and (e) the total reflectivity and (f) total transmissivity at both amplitudes.}
    \label{fig:alpha_results_1d_Ar2hAr}
  \end{center}
\end{figure}

\begin{figure}[tbp]
  \begin{center}
    \includegraphics{img/alpha/alpha_1d_hAr_Ar.png}
    \caption{Same as Fig.~\ref{fig:alpha_results_1d_Ar2hAr}, but from heavy Ar to Ar.}
    \label{fig:alpha_results_1d_hAr2Ar}
  \end{center}
\end{figure}

I repeat these simulations across the same range of wavevectors as used in the harmonic system in Section~\ref{sec:alpha_packets_validation}. In each simulation, I associate the initial frequency $\omega_0$ with the final spectra of reflected and transmitted energy in $\omega'$, which produces the reflectivity and transmissivity spectra plotted in Fig.~\ref{fig:alpha_results_1d_Ar2hAr} for Ar $\to$ heavy~Ar and in Fig.~\ref{fig:alpha_results_1d_hAr2Ar} for heavy~Ar $\to$ Ar. For the packets originating in Ar, the anharmonicity at the interface enables energy transfer into \emph{reflected} modes of frequency $\omega' = 2 \omega_0$, and even some into modes of frequency $\omega' = 3 \omega_0$. However, there is no appreciable energy transfer into \emph{transmitted} modes of different frequency. On the other hand, for packets originating in heavy~Ar, I observe negligible energy transfer into any frequencies $\omega' \neq \omega_0$. The only detectable inelastic energy transfer is some faint transmission into modes of frequencies $\omega' = 2 \omega_0$ in the range of $\omega' \sim 0.6$--0.8~THz in Fig.~\ref{fig:alpha_results_1d_hAr2Ar}d.

In both cases, the net effect of anharmonicity at the interface is to \emph{decrease} the transmissivity, albeit only slightly in the direction of heavy~Ar $\to$ Ar. The effect of inelastic scattering is evidently much more pronounced for packets originating in Ar than those originating in heavy~Ar. I attribute this to the fact that, for the same equivalent temperature, the heavy Ar atoms experience a smaller displacement, inversely proportional to the square root of their mass~\cite{Dove1993}. The heavy~Ar must therefore be raised to a higher equivalent temperature before the constituent atoms experience the same anharmonicity.

\subsection{In three dimensions}
\label{sec:alpha_results_3d}

The insights from the one-dimensional simulations may not necessarily be transferable to three-dimensional systems. In this section, I present results to help determine whether the observation that interfacial anharmonicity reduces transmissivity in 1D systems also holds in 3D systems, where the additional dimensionality allows more possibilites for mode coupling~\cite{Auld1973}. To that end, I performed simulations of wave packets at non-normal incidence in three-dimensional systems. In Fig.~\ref{fig:alpha_results_3d_v}, I show the $x$-components of atomic velocities during simulations of a wave packet with a wavevector of

\begin{equation}
  \mathbf{q}_0 = 0.3 \, \mathbf{q}_\text{max,[013]} = 0.3 \left( \frac{2 \pi}{3 a} \mathbf{y} + \frac{2 \pi}{a} \mathbf{z} \right),
\end{equation}
where $\mathbf{q}_\text{max,[013]}$ is the maximum wavevector in the [013] direction. Snapshots (a)--(c) show the transmission event in a system with all harmonic forces, while snapshots (d)--(f) are from a system that is harmonic but with LJ forces at the interface. Only the velocities of atoms in the (200) monolayer bisecting the wave packet are shown. I used GULP to calculate the three polarization vectors, and I chose one of the transverse-like modes:

\begin{equation}
  \mathbf{e}_{\mathbf{q}_0,\nu_0} = -0.876608 \, \mathbf{x} + 0.481205 i \, \mathbf{y}.
\end{equation}
In both packets I used the same maximum velocity of $B = 1.44$~\AA/ps, corresponding to $T_\text{equiv} = 50$~K. I calculated $\alpha_{\mathbf{q}_0,\nu_0}$ for each simulation, which is marked in plots (c) and (f). As in the 1D simulations, the transmissivity is lower in the system with anharmonic forces at the interface. However, no new mode is identifiable in plot (f).

\begin{figure}[tbp]
  \begin{center}
    \includegraphics{img/alpha/v_3d_Ar_pol2_harmonic_T50_1} \includegraphics{img/alpha/v_3d_Ar_pol2_T50_1} \\
    \includegraphics{img/alpha/v_3d_Ar_pol2_harmonic_T50_2} \includegraphics{img/alpha/v_3d_Ar_pol2_T50_2} \\
    \includegraphics{img/alpha/v_3d_Ar_pol2_harmonic_T50_3} \includegraphics{img/alpha/v_3d_Ar_pol2_T50_3}
    \caption[Atomic velocities during a simulation of wave packets in 3D systems with all harmonic forces vs.\ LJ bonds at the interface.]{$x$-components of atomic velocities during simulations of wave packets at non-normal incidence. The systems are three-dimensional with (a--c) all harmonic forces and (d--f) LJ bonds at the interface.}
    \label{fig:alpha_results_3d_v}
  \end{center}
\end{figure}

I repeated these simulations for the range of wavenumbers $\mathbf{q}_0 = [0.1, 0.2, 0.3, 0.35] \, \mathbf{q}_\text{max,[013]}$ and a range of amplitudes corresponding to $T_\text{equiv} = [10, 30, 50]$~K. The resulting transmissivity values are listed in Table~\ref{tab:alpha_results_3d}. In general, the behavior of $\alpha_{\mathbf{q}_0,\nu}$ in 3D looks similar to the behavior in 1D. As a function of wavenumber, the transmissivity is high and flat at low $q_0$, and it decreases sharply and monotonically as $q_0/q_\text{max,[013]}$ approaches 0.4, at which point the frequency in Ar surpasses the maximum frequency in heavy~Ar. As a function of amplitude, the transmissivity is practically independent of $T_\text{equiv}$ for low-$q_0$ modes, but decreases slightly with $T_\text{equiv}$ for high-$q_0$ modes. The preferential effect on high-$q_0$ modes is consistent with the fact that the anharmonicity is localized spatially to the interface, therefore most strongly affecting vibrations with short wavelengths.

\begin{table}[tbp]
  \caption{Transmissivity Values from 3D Wave Packet Simulations}
  \begin{center}
    \begin{tabular}{c | c c c c}
                               & \multicolumn{4}{c}{$\alpha_{\mathbf{q_0},\nu}$} \\
      $q_0/q_\text{max,[013]}$ & harmonic & 10~K & 30~K & 50~K \\
      \hline
      0.10                     & 0.86     & 0.86 & 0.86 & 0.86 \\
      0.20                     & 0.86     & 0.86 & 0.85 & 0.85 \\
      0.30                     & 0.72     & 0.71 & 0.70 & 0.66 \\
      0.35                     & 0.44     & 0.43 & 0.42 & 0.39 \\
    \end{tabular}
    \label{tab:alpha_results_3d}
  \end{center}
\end{table}

\section{Discussion}
\label{sec:alpha_discussion}

The results in both one- and three-dimensional systems confirm that anharmonicity in the atomic forces enables inelastic scattering processes at an interface. Those processes allow energy coupling among modes of different frequency, and the rate of the energy transfer does increase with the amplitude of the incident vibration. However, in all cases examined here, the inelastic processes increase energy transfer to \emph{reflected} modes, even in cases of inelastic \emph{transmission} (Fig.~\ref{fig:alpha_results_1d_hAr2Ar}), thereby decreasing the total transmissivity with increasing amplitude. Therefore, the results do not support the hypothesis that elevated conductance at high temperature arises due to increased transmissivity via inelastic phonon processes.

These conclusions provide interesting insight in the context of recent literature. As described in Sections~\ref{sec:intro_litreview_highh} and~\ref{sec:methods_interfaces_alpha}, the opening of ``inelastic channels'' at high temperatures is the primary explanation for experimental conductance measurements far in excess of the AMM and DMM~\cite{Stoner1992,Stoner1993,Lyeo2006,Hohensee2015}. Recently, S\"{a}\"{a}skilahti~\textit{et~al.}~\cite{Saaskilahti2014a} showed that inelastic processes do indeed occur at the interface during NEMD simulations, and furthermore that frequency-doubling and frequency-halving processes are dominant. This is consistent with the results of Section~\ref{sec:alpha_results}. However, they show that inelastic processes \emph{do} significantly enhance conductance, in apparent contradiction with my conclusions. In order to reconcile our findings, a comparison should be made between the single-mode transmission picture in my work and their inelastic conductance spectrum $g^\text{inel}(\omega,\omega')$ calculated during an actual NEMD simulation. As shown by the work of Kimmer~\textit{et~al.}~\cite{Kimmer2007} and Aubry~\textit{et~al.}~\cite{Aubry2008}, our existing theories of conductance are not yet mature enough to provide a quantitative connection between the transmission of isolated modes and the conductance of a thermal energy distribution.

In comparing the two pictures, one important difference is that the wave packet simulations of this chapter occur at ``zero temperature'' aside from the energy in the packet itself: the rest of the crystal is in its equilibrium state. Therefore, the energy propagating in the wave packet does not experience the same environment that it would during a thermal transport scenario. It is possible, then, that there are additional scattering processes which would occur during a full NEMD simulation, but which are suppressed during the wave packet simulation. This is a shortcoming of the wave packet approach. However, I am not aware of an alternative method to obtain the per-mode transmissivities $\alpha_{\mathbf{q}_0,\nu_0}$, as are desired for predictive conductance models, during simulations at (or near) thermal equilibrium. Furthermore, the shortcoming of the wave packet model does not detract from the conductance calculations of Chapter 3, which also suggest the same conclusion: that inelastic processes at the interface itself may not contribute much to the total conductance. In those simulations, energy occupies the normal modes in a near-equilibrium distribution (Section~\ref{sec:h_nemd_validation}), and would presumably undergo any processes that may not be observable in the present wave packet simulations. However, the resulting conductance values are still much lower that the conductances calculated in the systems with all LJ forces.

%In comparing the two pictures, often point out that wave packets occur at "zero K", so the energy in the mode isn't seeing the same environment that it would otherwise. So, in the cases simulated in this chapter, perhaps there are other possible channels that would occur during a full NEMD simulation, but are suppressed during the wave packet simulation. I concede that this is possible. However, I am not aware of a transparent way to get the per-mode transmissivity, as would be desired for a predictive conductance model, during a full NEMD simulation. Furthermore, that issue would not explain the results of Chapter 3, which also suggest the same thing: that inelastic scattering doesn't help conductance all that much. In that case, the normal mode vibrations are living in their natural habitat, but whatever extra processes they might have been experiencing still did not increase the conductance appreciably.

That being said, other recent computational work does corroborate the conclusion that inelastic processes at the interface might play a smaller role than expected. Wu and Luo~\cite{Wu2014} performed NEMD simulations while varying the third-order force constants in the bulk and at the interface. The conductance increased significantly with anharmonicity in the bulk, but did not change measurably with anharmonicity at the interface. Similarly, Murakami~\textit{et~al.}~\cite{Murakami2014} used both equilibrium MD and NEMD to show that the conductance is dominated by inelastic scattering not at the interface itself, but in a ``transition region'' near the interface in which scattered phonons rethermalize. This is related to the other commonly-mentioned explanation for excess thermal conductance: the non-equilibrium distribution of phonons, as introduced in Section~\ref{sec:methods_interfaces_n} and investigated further in Chapter~\ref{sec:n}.
