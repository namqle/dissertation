\chapter{Effects of Anharmonicity on Phonon Distribution}
\label{sec:n}

%At the outset of this work, I expected that the generally increasing trend of $h(T)$ seen in Chapter~\ref{sec:h} would be explained by an increase in the transmissivity $\alpha_{\mathbf{q},\nu}$ with temperature corresponding to an increase in inelastic processes. However, the results presented in Chapter~\ref{sec:alpha} suggest that this is not the case. Therefore, 
%In this chapter, I present results assessing the effect of anharmonicity on the phonon distribution $n_{\mathbf{q},\nu}$, which has also been identified in the literature as a potential underlying cause for increased conductance. 
Between the transmissivity $\alpha_{\mathbf{q},\nu}$ and the distribution $n_{\mathbf{q},\nu}$, the two phononic quantities suspected to be responsible for the thermal conductance trends calculated in Chapter~\ref{sec:h}, the results of Chapter~\ref{sec:alpha} suggest that $\alpha_{\mathbf{q},\nu}$ alone is unlikely to explain $h(T)$. In this chapter, I present results assessing the effect of anharmonicity on $n_{\mathbf{q},\nu}$. As the classical proxy for $n_{\mathbf{q},\nu}$, I calculate the spatial variation of the energy per mode, $E_{\mathbf{q},\nu}(z) = \hbar \omega \, n_{\mathbf{q},\nu}$, using the wavelet transform, described in Section~\ref{sec:n_wavelets}. I present calculations of $E_{\mathbf{q},\nu}(z)$ during 1D and 3D NEMD simulations in Section~\ref{sec:n_results}. In order to interpret the energy distributions, I also calculate the mode relaxation times $\tau_{\mathbf{q},\nu}$ in Ar and heavy Ar using the method of normal mode decomposition, described in Section~\ref{sec:tau_nmd}. I present those relaxation times in Section~\ref{sec:tau_results}, along with the corresponding mean free paths. Finally, in Section~\ref{sec:n_discussion}, I discuss a hypothesis to explain the conductance at high temperatures that is best supported by the available facts.

\section{The wavelet transform}
\label{sec:n_wavelets}

There are multiple options for analysis tools with which one can obtain the energy per mode, $E_{\mathbf{q},\nu}$. In particular, I seek to analyze the energy per mode in the NEMD simulations of Chapter~\ref{sec:h}, which are not in thermal equilibrium, so it is useful to consider the spatial variation of the energy per mode: $E_{\mathbf{q},\nu}(z)$. Therefore, I choose the wavelet transform as my primary analysis tool, as implemented for MD by Baker~\textit{et~al.}~\cite{Baker2012}, precisely because the wavelet spectrum $\tilde{w}(z,q)$\footnote{Given a signal $w(z)$, I write its wavelet transform with a tilde and its Fourier transform with a bar.} of a signal $w(z)$ preserves information in both $z$ and $q$. If, for example, $w(z)$ represents snapshots of atomic velocities during an NEMD simulation, then one can directly convert $\tilde{w}(z,q)$ to $E_{\mathbf{q},\nu}(z)$. In contrast, the spatial Fourier transform $\bar{w}(q)$ of $w(z)$ completely encodes any spatial variation into the amplitudes and phases of the plane-wave basis, making the $z$-dependence non-transparent. Another option to obtain $E_{\mathbf{q},\nu}(z)$ would be start with the atomic velocities over time, $w(t,z)$, and obtain the temporal Fourier transform, $\bar{w}(\omega,z)$. One could then convert $\bar{w}(\omega,z)$ to the spatial variation of energy per frequency, $E(\omega,z)$. However, extracting $E_{\mathbf{q},\nu}(z)$ from $E(\omega,z)$ is difficult, since the analysis in the time and frequency domains does not distinguish among different normal modes with the same frequency.

\subsection{Methodology}
\label{sec:n_wavelets_methodology}

The wavelet transform $\tilde{w}(q,z)$ of a signal $w(z)$ takes the same form as other integral transforms, such as the Fourier and Laplace transforms:

\begin{equation}
  \label{eqn:n_wavelets_W}
  \tilde{w}(z',q') = \mathcal{W} \{ w(z) \} = \int_{-\infty}^\infty w(z) \, \psi_{z',q'}^* (z) \, dz.
\end{equation}
The difference lies in the kernel functions $\psi_{z',q'}$. Different definitions of wavelet-type kernel functions are possible. I follow the convention of Baker~\textit{et~al.}~\cite{Baker2012} in which each ``daughter wavelet,'' corresponding to a specific location $z'$ and wavenumber $q'$, is defined as

\begin{equation}
  \label{eqn:n_wavelets_psi}
  \psi_{z',q'}(z) = \pi^{-1/4} \, {\left( \frac{q'}{q_0} \right)}^{1/2} \, \exp \left[ i q' (z - z') \right] \, \exp \left[ - \frac{1}{2} {\left( \frac{q'}{q_0} \right)}^2 {(z - z')}^2 \right],
\end{equation}
which is a scaled and translated version of a ``mother wavelet'' $\psi_{z',q_0}$ whose dominant wavenumber is $q_0$. This definition is normalized so that the energy density per length, per wavenumber is conveniently calculated as

\begin{equation}
  \label{eqn:n_wavelets_energy}
  E_\psi(z',q') = \frac{1}{C q_0} |\tilde{w}(z',q')|^2.
\end{equation}
The constant $C$ accounts for the fact that, unlike the plane waves that form the basis functions for the Fourier transform, the wavelets are not orthogonal:

\begin{equation}
  \label{eqn:n_wavelets_C}
  C = \int_{-\infty}^{\infty} \frac{|\bar{\psi}_{z',q_0}(q)|^2}{|q|} dq,
\end{equation}
where $\bar{\psi}_{z',q_0'}(q)$ is the Fourier spectrum of the mother wavelet $\psi_{z',q_0}(z)$. Equations~\ref{eqn:n_wavelets_W}--\ref{eqn:n_wavelets_C} suffice to discuss the results of this chapter. For additional details on practical implementation of the transform, refer to Refs.~\cite{Baker2012,Baker2015}.

The results presented in this chapter were obtained from the same NEMD simulations presented in Chapter~\ref{sec:h}. All atomic velocities were written to disk every 40~ns (20\,000 steps) in the same data collection period during which the temperature profiles were collected. Based on the atomic velocities, I then used the combination $w(z) = \sqrt{m(z)/2} \; v(z)$ as the signal to be transformed, so that the wavelet energy density calculated by Eq.~\ref{eqn:n_wavelets_energy} corresponds to the density of kinetic energy per length, per wavenumber. To reduce noise, each of the energy distributions reported below is the average of distributions calculated from ten independent simulations under the same conditions.

In the 1D systems, the equilibrium site of each atom has a unique value of $z$, so the atomic velocities can be translated directly into $w(z)$. In the 3D systems, I average together the velocities of atoms in the same (002) plane (i.e.,\ those whose equilibrium positions have the same $z$ coordinate) to form the signal $w(z)$. This is equivalent to sampling only the normal modes with $\mathbf{q}$ parallel to the $\langle 001 \rangle$ direction. The different polarizations $\nu$ can be sampled by selecting the corresponding component of atomic velocities in calculating $w(z)$. For example, to probe the energy density of a longitudinal mode along $\langle 001 \rangle$, I use the $z$-component of the atomic velocities to construct $w(z)$. In the practical implementation of the transform, I choose the wavenumber of the mother wavelet as $q_0 = {(5 a)}^{-1}$ and minimum and maximum wavenumbers corresponding to constants $\eta = 0.05$ and $\phi = 1$ as described by Baker~\textit{et~al.}~\cite{Baker2012}. These settings produce energy spectra with useful information in the range of wavenumbers between $q / q_\text{max} = 0.04$ and~0.7.

To facilitate interpretation, in all of the following results, I use the equipartition principle to convert the energy density at each point, $E^\text{K}(z,q)$, to an ``equivalent'' temperature $T_\text{equiv}(z,q)$ in K. This would be the temperature of a system with an equal energy density distributed uniformly; i.e.,\ at thermal equilibrium.

\subsection{Verification}
\label{sec:n_wavelets_validation}

In order to verify that the method has been correctly implemented, and to establish a basis for comparison, I calculate the energy density in a system at equilibrium. As a test system, I simulate an interface between 3D Ar/heavy Ar with LJ forces at $T_\text{nominal} = 2$~K, but with zero heat current applied. The resulting energy distribution is shown in Fig.~\ref{fig:n_wavelets_validation}a in terms of $T_\text{equiv}(z,q)$.

\begin{figure}[tbp]
  \begin{center}
    \includegraphics{img/n/E_validation_3d}
    \includegraphics{img/n/E_validation_3d_hist}
    \caption[The energy distribution from the wavelet tranform in a system at thermal equilibrium.]{(a) The energy distribution in terms of equivalent temperature, $T_\text{equiv}$, calculated using the wavelet transform in a system at thermal equilibrium. (b) A histogram of $T_\text{equiv}$.}
    \label{fig:n_wavelets_validation}
  \end{center}
\end{figure}

As expected for a system at thermal equilibrium, the energy appears relatively evenly distributed both in space (horizontal axis) and among normal modes, as enumerated by wavenumbers (vertical axis). The true average temperature is $T = 1.86$~K, slightly lower than the nominal temperature of 2~K because of the simple method for setting atomic velocities (Section~\ref{sec:h_nemd_methodology}). To capture the magnitude of fluctuations in the data, I show a histogram of the calculated values of $T_\text{equiv}(z,q)$ in Fig.~\ref{fig:n_wavelets_validation}b. The fluctuations in the measured energy distribution are equivalent to temperature fluctuations of $\sim 0.1$~K, which are sufficiently small to resolve the non-equilibrium effects presented in the next section, specifically in Figs.~\ref{fig:n_results_1d} and~\ref{fig:n_results_3d}.

\section{Results: Energy distributions}
\label{sec:n_results}

In the following sections, I present the distributions of kinetic energy calculated by the wavelet transform in one- and three-dimensional NEMD simulations.

\subsection{In one dimension}
\label{sec:n_results_1d}

The distributions of energy density in six 1D NEMD simulations are shown in Fig.~\ref{fig:n_results_1d}. Each row was collected at the same $T_\text{nominal}$: (a,b)~2~K, (c,d)~6~K, and (e,f)~12~K, corresponding to the temperature range of conductance data presented in Section~\ref{sec:h_results_1d}. Refer to Fig.~\ref{fig:h_results_Tz_1d} to see the temperature profiles $T(z)$ corresponding to the simulations at $T_\text{nominal} = 6$~K. The left column (a,c,e) shows the distributions arising in systems with LJ forces, and the right column (b,d,f) shows the distributions in harmonic systems.

\begin{figure}[htbp]
  \begin{center}
    \setlength{\tabcolsep}{0pt}
    \begin{tabular}{c c}
      LJ forces & Harmonic forces \\
      \includegraphics{img/n/E_1d_T02_lj} &
      \includegraphics{img/n/E_1d_T02_harmonic} \\
      \includegraphics{img/n/E_1d_T06_lj} &
      \includegraphics{img/n/E_1d_T06_harmonic} \\
      \includegraphics{img/n/E_1d_T12_lj} &
      \includegraphics{img/n/E_1d_T12_harmonic} \\
    \end{tabular}
    \caption[Energy distributions calculated using the wavelet transform during 1D NEMD simulations at 2~K, 6~K, and 12~K.]{Energy distributions, represented in terms of equivalent temperature, calculated using the wavelet transform during 1D NEMD simulations at (a,b)~2~K, (c,d)~6~K, and (e,f)~12~K.}
    \label{fig:n_results_1d}
  \end{center}
\end{figure}

At low temperature (2~K), the LJ and harmonic systems have a similar energy distribution. There is an excess of energy in the Ar side in modes with wavenumbers higher than $\sim 0.4 \, q_\text{max}$. This can be understood by referring to the dispersion relation (Eq.~\ref{eqn:methods_normalmodes_ld_dispersion}), which shows that given the mass ratio of 3 between the two materials, the highest frequency in heavy Ar corresponds to a wavenumber of

\begin{equation*}
  \frac{q}{q_\text{max}} = \frac{2}{\pi} \, \arcsin ( 3^{-1/2} ) \approx 0.39
\end{equation*}
in the Ar. As the temperature increases, (c,e) anharmonic phonon scattering in the LJ system allows the energy to redistribute along the $q$ axis, while (d,f) the lack of phonon scattering in the harmonic system prevents the excess energy from thermalizing. Note that in panels (c,e), the energy distribution has not quite completely thermalized among the normal modes in the heavy Ar. This indicates that the mean free paths in the heavy Ar are longer than in the Ar, which is consistent with the lower temperature and the normal mode decomposition calculations presented in Section~\ref{sec:tau_results_1d}.

%In Chapter~\ref{sec:h}, the conductance was also calculated in systems with all harmonic forces except at the interface, where I used LJ bonds. The energy distribution among the modes in those systems look nearly identical to the systems with all harmonic forces, and I have not reproduced them here.

\subsection{In three dimensions}
\label{sec:n_results_3d}

I have also calculated the energy density in 3D simulations. Although in principle it is possible to calculate the energy density for each mode in the full Brillouin zone, for simplicity and ease of comparison with the 1D simulations, I only present here the energy density in longtitudinal and transverse modes along the $\langle 001 \rangle$ direction. The resolution in both $z$ and $q$ is low, since each signal represents the velocities of only 72 planes, versus 11232 atoms in the 1D systems.\footnote{The atoms in the immobile walls are excluded from the signals.} However, one can still see that, as in the 1D systems, the LJ and harmonic systems have similar distributions of energy at low temperature, with an excess of energy in Ar above $0.4 \, q_\text{max}$. As the temperature increases, the energy thermalizes in the LJ system but remains confined to the high-wavenumber modes in the harmonic system. %However, the isolation of energy in those modes does not seem quite as severe as in 1D. Qualitatively, this seems to corroborates the idea that more phonon--scattering events are possible in 3D than in 1D; see also the results of Section~\ref{sec:tau_results_1d}.

\begin{figure}[htbp]
  \begin{center}
    \setlength{\tabcolsep}{0pt}
    \begin{tabular}{c c}
      LJ forces & Harmonic forces \\
      \includegraphics{img/n/E_3d_T02_lj} &
      \includegraphics{img/n/E_3d_T02_harmonic} \\
      \includegraphics{img/n/E_3d_T30_lj} &
      \includegraphics{img/n/E_3d_T30_harmonic} \\
      \includegraphics{img/n/E_3d_T50_lj} &
      \includegraphics{img/n/E_3d_T50_harmonic} \\
    \end{tabular}
    \caption[Energy distributions calculated using the wavelet transform during 3D NEMD simulations.]{Energy distributions, represented in terms of equivalent temperature, calculated using the wavelet transform during 1D NEMD simulations at (a,b)~10~K, (c,d)~30~K, and (e,f)~50~K.}
    \label{fig:n_results_3d}
  \end{center}
\end{figure}

\section{Normal mode decomposition}
\label{sec:tau_nmd}

To interpret the energy distributions presented in Section~\ref{sec:n_results}, it is useful to know the mean free paths of modes in the bulk materials. In this section, I use normal mode decomposition (NMD) to calculate those mean free paths over the ranges of temperatures encountered in the NEMD simulations. For a thorough discussion of the method, refer to the recent review by McGaughey and Larkin~\cite{McGaughey2014}. In the following overview, I survey the concepts necessary to interpret the results presented in Section~\ref{sec:tau_results}.

\subsection{Methodology}
\label{sec:tau_nmd_methodology}

Broadly speaking, the NMD method consists of the following steps:

\begin{enumerate}
  \item Specify the atomic structure of the material of interest and perform lattice dynamics calculations (Section~\ref{sec:methods_normalmodes_ld}) to obtain $\omega_{\mathbf{q},\nu}$ and $\mathbf{e}_{\mathbf{q},\nu}$ of each mode in the harmonic system (i.e.,\ the low temperature limit).
  \item Perform an MD simulation of the same structure at thermal equilibrium, recording snapshots of $r_{i,\alpha}$ and $\dot{r}_{i,\alpha}$ at a sufficiently high rate to capture the highest-frequency modes and for a sufficiently long duration to reduce statistical noise.
  \item Use those data, along with the normal mode properties from Step 1, to calculate $\xi_{\mathbf{q},\nu}(t)$ and $\dot{\xi}_{\mathbf{q},\nu}(t)$.
  \item Calculate the autocorrelation of $\xi_{\mathbf{q},\nu}(t)$ and $\dot{\xi}_{\mathbf{q},\nu}(t)$ in time. In cases where the single-mode relaxation time approximation (SMRTA) is accurate (Eq.~\ref{eqn:methods_bulk_bte_smrta}), the autocorrelation function will closely resemble an exponentially damped oscillation in the time domain or, equivalently, a Lorentzian peak in the frequency domain (see Appendix~A.1 of Ref.~\cite{Larkin2013a}). In this work, I use the frequency domain, and the width of the peak is inversely proportional to the total relaxation time $\tau_{\mathbf{q},\nu}$.
\end{enumerate}

The analysis using the normal mode displacements, $\xi$, is problematic at high temperatures, where their contribution to the system energy is increasingly anharmonic and the equipartition theorem breaks down~\cite{McGaughey2014}. Therefore, in this work I only use the normal mode velocities, $\dot{\xi}$, whose contribution to the Hamiltonian is always harmonic. To clarify Step 4: after calculating $\dot{\xi}_{\mathbf{q},\nu}(t)$ of each mode using Eq.~\ref{eqn:methods_normalmodes_r2xi}, I use it to calculate the kinetic energy spectrum in that mode:

\begin{equation}
  \label{eqn:tau_nmd_EK}
  E^\text{K}_{\mathbf{q},\nu}(\omega) = \frac{1}{2 \pi t_\text{f}} \left| \bar{\dot{\xi}}_{\mathbf{q},\nu}(\omega) \right|^2,
\end{equation}
where $t_\text{f}$ is the duration of the sampled signal $\dot{\xi}_{\mathbf{q},\nu}(t)$, and the bar denotes the Fourier transform $\bar{\dot{\xi}}_{\mathbf{q},\nu}(\omega) = \int_0^{t_\text{f}} \dot{\xi}_{\mathbf{q},\nu}(t) \exp (-i \omega t) \, dt$. Equation~\ref{eqn:tau_nmd_EK} is normalized so that, given a duration $t_\text{f}$ much longer than the timescale of oscillations, the average kinetic energy in the mode is

\begin{equation}
  \langle E^\text{K}_{\mathbf{q},\nu} \rangle = \int_0^\infty E^\text{K}_{\mathbf{q},\nu}(\omega) \, d\omega.
\end{equation}
If the SMRTA is valid, then the ``measured'' energy spectrum calculated according to Eq.~\ref{eqn:tau_nmd_EK} will resemble a Lorentzian function given by

\begin{equation}
  \label{eqn:tau_nmd_lorentzian}
  E^\text{K}_{\mathbf{q},\nu}(\omega) = \frac{1}{2} k_\text{B} T \, \frac{\Gamma_{\mathbf{q},\nu}/\pi}{{(\omega-\omega_{\mathbf{q},\nu})}^2 + \Gamma_{\mathbf{q},\nu}^2},
\end{equation}
where I have used the fact that, in a system at thermal equilibrium, $\langle E^\text{K}_{\mathbf{q},\nu} \rangle = \frac{1}{2} k_\text{B} T$. The frequency of the normal mode is $\omega_{\mathbf{q},\nu}$, and the total relaxation time is $\tau_{\mathbf{q},\nu} = {(2 \Gamma_{\mathbf{q},\nu})}^{-1}$.

I have used Eqs.~\ref{eqn:tau_nmd_EK} and~\ref{eqn:tau_nmd_lorentzian} to determine the normal mode frequencies and relaxation times in the simulated Ar and heavy Ar systems whose parameters are given in Section~\ref{sec:methods_thiswork}. Each system is specified by its dimensionality, material, and temperature: e.g.,\ 3D Ar at 20~K. In all calculations, each simulation domain has periodic boundary conditions, and the data are collected at thermal equilibrium. The 1D systems consist of 5000 atoms ($\sim 1.33$~$\upmu$m), and the 3D systems consist of 4000 atoms ($\sim (5.31$~nm$)^3$). Although the size of the 3D system appears small compared to the mean free paths presented in Section~\ref{sec:tau_results_3d}, the periodic boundary conditions ensure that no boundary scattering occurs, so it is possible to simulate systems using domains smaller than the mean free path. The results presented in Ref.~\cite{McGaughey2014} show that the system size of $(10a)^3$ is sufficiently large to reproduce bulk transport behavior. I chose to analyze 400 modes in the 1D system and 435 modes in the 3D system, all sampled uniformly throughout the first Brillouin zone. In 3D, the modes correspond to the 145 $q$-points in the irreducible Brillouin zone with spacing $\Delta q_\alpha = 2 \pi / L_\alpha = \pi / (5 a)$.

For each system, I performed ten simulations under identical conditions except with a different initial configuration of randomized velocities. Each simulation ran for 2$^{20}$ timesteps ($\sim 2$~ns) to equilibrate before data collection, at which point the atomic velocities were sampled once every 32~timesteps, corresponding to a maximum observable frequency of 15.6~THz, for a duration of another 2$^{20}$ timesteps. These are similar to the settings used in Ref.~\cite{McGaughey2014}. Finally, for each mode in the system, I obtained $E^\text{K}_{\mathbf{q},\nu}(\omega)$ in the ten simulations using Eq.~\ref{eqn:tau_nmd_EK} and averaged them together to fit to the form of Eq.~\ref{eqn:tau_nmd_lorentzian}.

I found that the following fitting procedure provided good results. The frequencies and linewidths were obtained by fitting $\log \left[ E^\text{K}_{\mathbf{q},\nu}(\omega) \right]$, which provides better sensitivity than fitting $E^\text{K}_{\mathbf{q},\nu}(\omega)$ itself. Then, Eq.~\ref{eqn:tau_nmd_lorentzian} was fit to the data points within 2 orders of magnitude of the maximum value in a given spectrum. Finally, the points within $\pm20$\% of the fitted value of $\omega_{\mathbf{q},\nu}$ were used to re-fit the equation, yielding final values of~$\omega_{\mathbf{q},\nu}$ and~$\Gamma_{\mathbf{q},\nu}$.

\subsection{Verification}
\label{sec:tau_nmd_validation}

In order to verify my implementation of the method, I reproduced the calculation in Ref.~\cite{McGaughey2014} for the relaxation times in LJ argon at a temperature of 10~K. To facilitate comparison, for this calculation I used a matching cutoff distance of $r_\text{cut} = 2.5\sigma = 8.378$~\AA\ in the LJ potential, rather than the value of $r_\text{cut} = 4.5$~\AA\ used elsewhere in this work to include only nearest neighbors. All other settings mentioned in the previous section are the same as those of Ref.~\cite{McGaughey2014}. Two examples of calculated $|\bar{\dot{\xi}}_{\mathbf{q},\nu}(\omega)|/(2 \pi t_\text{f})$ (colored dots) and the corresponding fitted Lorentzians (colored lines) are shown in Fig.~\ref{fig:tau_nmd_validation}a. The spectra are fit well by Eq.~\ref{eqn:tau_nmd_lorentzian} over four orders of magnitude, indicating that the SMRTA is valid for this system. For each mode, the frequency calculated from harmonic lattice dynamics is also plotted (gray lines). As expected, the actual normal mode frequencies are close to the harmonic values, but slightly lower due to anharmonic softening at elevated temperature. Similar fits were obtained for all of the sampled modes, and the resulting frequencies and relaxation times are shown in Fig.~\ref{fig:tau_nmd_validation}b. They are normalized by the LJ time scale, $\sqrt{m \sigma^2 / \epsilon}$, for direct comparison with Fig.~3 of Ref.~\cite{McGaughey2014}. The data are very closely reproduced.

\begin{figure}[tbp]
  \begin{center}
    \includegraphics{img/nmd/mcgaughey_EK}
    \includegraphics{img/nmd/mcgaughey_tau}
    \caption[NMD results for the system simulated by McGaughey and Landry.]{NMD results for the same system simulated in Ref.~\cite{McGaughey2014}. Compare with their Fig.~3. (a) Calculated and fitted spectra of kinetic energy (dots and lines) in two modes with their harmonic frequencies (vertical lines). (b) The relaxation times of all sampled modes.}
    \label{fig:tau_nmd_validation}
  \end{center}
\end{figure}

As a final point, I performed the same calculations in the 3D LJ Ar material with only nearest-neighbor interactions at $T = (5, 15, 25, 35, 45)$~K to obtain the dispersion relations $\omega_{\mathbf{q},\nu}(\mathbf{q})$ as a function of temperature. Those results are plotted in Fig.~\ref{fig:methods_thiswork_dispersion} and discussed in Section~\ref{sec:methods_thiswork}. The normal mode frequencies agree remarkably well with experimental measurements of the dispersion in real Ar at 4~K, and they exhibit the expected softening with temperature.

\section{Results: Relaxation times}
\label{sec:tau_results}

With the NMD method established, I use it here to obtain the relaxation times of normal modes in Ar and heavy Ar over the same temperature ranges used in the NEMD simulations of Chapter~\ref{sec:h}. I present the results separately for the one- and three-dimensional systems.

\subsection{In one dimension}
\label{sec:tau_results_1d}

I have fitted the frequencies and relaxation times of 1D Ar and heavy Ar at temperatures ranging from 2~K to 12~K. Two such fits to data at 8~K are shown in Fig.~\ref{fig:tau_results_EK}a. The collected spectra are only well fit by Lorentzian functions over roughly one order of magnitude, which shows that the SMRTA is less valid in 1D systems than in 3D systems. This is not entirely surprising, since researchers have long recognized that thermal transport in one-dimensional chains is poorly behaved and has a deep research field of its own---for example, see Refs.~\cite{Peierls1929,Fermi1955,Payton1967,Bishop1981,G2011,Berman2005,Li2012e,Meier2014}---that is well beyond the scope of this work. The main issue relevant here is that phonon--phonon processes are limited in 1D chains due to their low dimensionality, leading to phenomena such as divergent thermal conductivity with system length~\cite{Peierls1929,Meier2014}. For the present purposes, the limitation on possible phonon--phonon scattering processes is a likely explanation for the imperfect fitting of the kinetic energy spectra to Lorentzian peaks.

\begin{figure}[tbp]
  \begin{center}
    \includegraphics{img/nmd/EK_1d}
    \includegraphics{img/nmd/EK_3d}
    \caption{Energy spectra in two arbitrary normal modes sampled from (a) one-dimensional LJ Ar at 8~K and (b) three-dimensional LJ Ar at 35~K.}
    \label{fig:tau_results_EK}
  \end{center}
\end{figure}

Nevertheless, in the absence of a better model for the normal mode energy spectra, fitting to Eq.~\ref{eqn:tau_nmd_lorentzian} provides a reasonable estimate of the timescale of energy relaxation. This is similar in spirit to the estimates in Ref.~\cite{McGaughey2014} using the virtual crystal approximation to extract the relaxation times in random alloys. The resulting relaxation times are shown in Figure~\ref{fig:tau_results_1d}. They encompass 3--500~ps in Ar at 2~K and 4--1000~ps in heavy Ar at 2~K. In each system, the relaxation time falls roughly as $\omega^{-1}$ at low frequencies and more steeply at high frequencies. Increasing temperature also shortens the relaxation time, roughly as $T^{-1}$. As the temperature increases past $\sim 8$~K, the relaxation times in both systems begin to grow shorter than the periods of oscillation of the normal modes, as indicated on the plots by gray lines.

The relaxation time of each mode can then be readily transformed into a mean free path using knowledge of the group velocity: $\Lambda_{\mathbf{q},\nu} = |\mathbf{v}_{\mathbf{q},\nu}| \tau_{\mathbf{q},\nu}$. In the one-dimensional chain, the group velocity can be calculated analytically based on the sinusoidal dispersion relation, Eq.~\ref{eqn:methods_normalmodes_ld_dispersion}. The resulting mean free paths are shown in Figure~\ref{fig:tau_results_1d_mfp}. Over the range of simulated temperatures, the mean free path of most normal modes decreases by an order of magnitude or more. The mean free paths of the lowest-frequency modes are in the range of hundreds of~nm at $T = 2$~K and decrease to tens of~nm at $T = 12$~K.

\begin{figure}[p]
  \begin{center}
    \includegraphics{img/nmd/tau_1d_m40}
    \includegraphics{img/nmd/tau_1d_m120}
    \caption[Relaxation times of normal modes in one-dimensional Ar and heavy Ar.]{Relaxation times for normal modes in one-dimensional Ar (blue to red) and heavy Ar (cyan to orange) at temperatures $T = [2, 4, 6, 10, 12]$~K.}
    \label{fig:tau_results_1d}
  \end{center}
\end{figure}

\begin{figure}[p]
  \begin{center}
    \includegraphics{img/nmd/mfp_1d}
    \caption[Mean free paths of normal modes in one-dimensional Ar and heavy Ar.]{Mean free paths for normal modes in one-dimensional Ar (blue to red) and heavy Ar (cyan to orange) at temperatures $T = [2, 4, 6, 10, 12]$~K.}
    \label{fig:tau_results_1d_mfp}
  \end{center}
\end{figure}

\subsection{In three dimensions}
\label{sec:tau_results_3d}

Likewise, I have fitted the frequencies and relaxation times of 3D Ar and heavy Ar at temperatures ranging from 5~K to 45~K. Two such fits to normal mode spectra at 35~K are shown in Fig.~\ref{fig:tau_results_EK}b. These spectra are better modeled by Eq.~\ref{eqn:tau_nmd_lorentzian} than the spectra in the 1D system.

The relaxation times of all of the sampled modes are shown in Figure~\ref{fig:tau_results_3d}. They encompass 10--200~ps in Ar at 5~K and 20--400~ps in heavy Ar at 5~K. The relaxation time falls roughly as $\omega^{-2}$ at low frequencies, as expected~\cite{Callaway1959,Holland1963}, but exhibits a more complicated dependence at higher frequencies. I then convert these relaxation times into mean free paths, plotted in Fig.~\ref{fig:tau_results_3d_mfp}. Unfortunately, the group velocities $\mathbf{v}_{\mathbf{q},\nu} = \nabla \omega_{\nu}(\mathbf{q})$ throughout the 3D Brillouin zone are more difficult to calculate analytically than in 1D (see Section~3.4 of Ref.~\cite{McGaughey2014}), and the $q$-point density in the $V = {(10a)}^3$ system is too coarse to numerically approximate the gradient by finite difference. Therefore, I only obtain the mean free paths of modes along the $\langle 100 \rangle$ and $\langle 111 \rangle$ directions for which Eq.~\ref{eqn:methods_normalmodes_ld_dispersion} is valid, given the correct interplanar force constants~\cite{Dove1993}. The mean free paths decrease by an order of magnitude over the range of simulated temperatures, similar to the decrease observed in 1D systems. The mean free paths of the lowest-frequency modes are in the range of 100~nm at $T = 5$~K, and they decrease to tens of~nm at $T = 45$~K.

\begin{figure}[p]
  \begin{center}
    \includegraphics{img/nmd/tau_3d_m40}
    \includegraphics{img/nmd/tau_3d_m120}
    \caption[Relaxation times of normal modes in three-dimensional Ar and heavy Ar.]{Relaxation times for normal modes in three-dimensional Ar (blue to red) and heavy Ar (cyan to orange) at temperatures $T = [5, 15, 25, 35, 45]$~K.}
    \label{fig:tau_results_3d}
  \end{center}
\end{figure}

\begin{figure}[p]
  \begin{center}
    \includegraphics{img/nmd/mfp_3d_m40}
    \includegraphics{img/nmd/mfp_3d_m120}
    \caption[Mean free paths of normal modes along the $\langle 100 \rangle$ and $\langle 111 \rangle$ directions in three-dimensional Ar and heavy Ar.]{Mean free paths for normal modes along the $\langle 100 \rangle$ and $\langle 111 \rangle$ directions in three-dimensional Ar (blue to red) and heavy Ar (cyan to orange) at temperatures $T = [2, 25, 45]$~K.}
    \label{fig:tau_results_3d_mfp}
  \end{center}
\end{figure}

\section{Discussion}
\label{sec:n_discussion}

The results presented in this chapter enable further discussion on two issues: (1) the transition from ballistic to diffusive transport with increasing temperature and (2) the role of the phonon distribution in conductance at high temperature.

\subsection{Ballistic and diffusive transport}
\label{sec:n_discussion_ballistic}

First, the energy distributions presented in Section~\ref{sec:n_results} show that, in both the 1D and 3D systems, the transport is ballistic in simulations at the lowest $T_\text{nominal}$ and diffusive at the highest. Some ballistic-to-diffusive transition must occur in the intervening temperatures, and the mean free paths presented in Section~\ref{sec:tau_results} provide information to elucidate that transition. The temperature trends in the energy distributions are \emph{qualitatively} consistent with those in the mean free paths---as temperature increases in the LJ system, the mean free path decreases and the energy distribution tends to equilibrate among the normal modes.

However, I am not fully confident in the \emph{quantitative} comparison between the two calculations. The calculations appear to be consistent in the 3D system, but not in the 1D system. For example, in the 3D system at $T_\text{nominal} = 10$~K, the energy in modes above $q/q_\text{max} \sim 0.4$ appears to have relaxed partially, but not completely, indicating that the mean free paths are on the same order of magnitude as the lead length of 20~nm. Indeed, the corresponding mean free paths in Fig.~\ref{fig:tau_results_3d_mfp} are about 10~nm. On the other hand, the 1D system at $T_\text{nominal} = 6$~K also exhibits a partially-ballistic, partially-diffuse energy distribution. However, the corresponding mean free paths in Fig.~\ref{fig:tau_results_1d_mfp} are only a few~nm, much shorter than the lead length of 1.5~$\upmu$m. In principle, it may be possible in the future to make this comparison more rigorously by using the relaxation times as input to iterative solutions of the Boltzmann transport equation (Eq.~\ref{eqn:methods_bulk_bte_solution}) to obtain $n_{\mathbf{q},\nu}$, and hence $E_{\mathbf{q},\nu}$. This approach is not new~\cite{Minnich2015}, but as mentioned in Section~\ref{sec:methods_interfaces_n}, applying it to interfaces requires the transmissivity $\alpha_{\mathbf{q},\nu}$ as input, for which there is still little consensus on effective models. Therefore, at present, it is unclear whether solutions to the BTE can provide physically meaningful results in interface-dominated transport.

When compared with the results of Chapter~\ref{sec:h}, it also appears that the shift from ballistic to diffusive transport may be associated with a change in the trend of $h(T)$. The energy distributions show that the transition occurs around roughly~6 to~8~K in the 1D systems (Fig.~\ref{sec:n_results_1d}) and around 20~K in the 3D systems (Fig.~\ref{sec:n_results_3d}). The trends in $h(T)$ also appear to change behavior near those temperatures. This aligns with the findings of Liang and Keblinski~\cite{Liang2014,Liang2014a}, who investigated the effect of ballistic transport on interfacial conductance by inducing diffusive scattering at the far walls.

\subsection{The phonon distribution and thermal conductance}
\label{sec:n_discussion_hT}

Although the ballistic effects on interfacial conductance are interesting in certain applications, such as in superlattices~\cite{Chen1998}, they are also often treated as an artifact in determining the ``true'' conductance under a thermal distribution of phonons. It is therefore preferable to conduct simulations in the diffusive regime, in which scattering mechanisms are strong enough to thermalize the phonon distribution. In that context, the results of this chapter help identify the subset of simulations in which transport is diffusive: evidently, the 1D simulations at $T_\text{nominal} \gtrsim 8$~K and the 3D simulations at $T_\text{nominal} \gtrsim 20$~K. In those regimes, the values of $h(T)$ calculated in Chapter~\ref{sec:h} increase with temperature roughly linearly, which corroborates previous findings in MD~\cite{Stevens2007,Landry2009a}.

In this section, I seek a qualitative explanation for this behavior in $h(T)$ that is related to $n_{\mathbf{q},\nu}$, since the results of Chapter~\ref{sec:alpha} suggest that the other suspected culprit, $\alpha_{\mathbf{q},\nu}$, is unlikely to be responsible. However, within the noise of the collected data, I observe the apparently uninteresting behavior that $n_{\mathbf{q},\nu} \to n_\text{B--E}(\omega_{\mathbf{q},\nu},T)$ as $T$ increases in the diffusive regime: see Figs.~\ref{fig:n_results_1d}e and~\ref{fig:n_results_3d}e. Therefore, in agreement with Landry and McGaughey~\cite{Landry2009a}, I find no support in my simulations for Simons' correction~\cite{Simons1974} to the distribution to account for disruption by a non-zero heat current. My results do, however, support the idea that the distribution is strongly disrupted by the \emph{interface}, albeit at low temperatures, not high temperatures.

The physical picture most consistent with these results can be seen as a conglomeration of models described by Duda~\textit{et~al.}~\cite{Duda2011}, Wu and Luo~\cite{Wu2014}, and Murakami~\textit{et~al.}~\cite{Murakami2014} to explain the results of molecular dynamics simulations. In this picture, the transmissivity remains relatively unchanged as temperature increases. However, the temperature increases the rate of inelastic phonon processes in the \emph{bulk materials}, which are responsible for the anomalous conductance.

The results presented in Chapters~\ref{sec:h},~\ref{sec:alpha}~, and~\ref{sec:n} in the 1D and 3D Ar/heavy Ar system are consistent with this picture. The transmissivity does not change much with increasing vibrational amplitude and therefore ``equivalent temperature'' (Section~\ref{sec:alpha_results}). Notably, the maximum frequency of transmission stays fixed at 1.15~THz, which is the highest frequency of any modes in the heavy Ar. Therefore, the phonons reflected back into the Ar lead have a highly non-equilibrium distribution, with excess energy in modes above $q/q_\text{max} = 0.4$ (Section~\ref{sec:n_results}). That reflected energy must transfer into modes of lower wavenumber/frequency before it can later transmit through the interface. At low temperatures, such processes happen very slowly, and the reflected energy can only contribute at a slow rate to the flux through the interface. As temperature increases through the ranges simulated in this work, the relaxation times decrease by an order of magnitude or more (Section~\ref{sec:tau_results}), which allows the non-equilibrium distribution of phonons to rethermalize more rapidly. This enables the excess high-frequency phonons to transfer energy at a faster rate to lower modes and transmit through the interface, leading to a higher thermal conductance (Section~\ref{sec:h_results}).
