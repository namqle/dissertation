\chapter{Effects of Anharmonicity on Thermal Conductance}
\label{sec:h}

%\begin{quote}
%  It seems possible to account for all the phenomena of heat, if it be supposed that in solids the particles are in a constant state of vibratory motion, the particles of the hottest bodies moving with the greatest velocity, and through the greatest space.
%
%  \raggedleft{--- Sir Humphry Davy, \text{Elements of Chemical Philosophy}, 1812~\cite{Davy1812}}
%\end{quote}
%\begin{quote}
%  The atomistic philosophy, with its childish and superfluous subsidiary conceptions, is in peculiar contrast with the remaining philosophical development of the physics of today.
%
%  \raggedleft{--- Ernst Mach, \textit{Principles of the Theory of Heat}, 1896~\cite{Mach1896}}
%\end{quote}

In this chapter, I focus on observing the macroscopic heat transfer at the interface, as summarized in the thermal conductance. Specifically, I show how the thermal conductance changes depending on the anharmonicity of the interatomic forces throughout the system---both at the interface and in the bulk materials. I calculate the conductance using the non-equilibrium molecular dynamics method, described in Section~\ref{sec:h_nemd}. I present the results of the one-dimensional and three-dimensional simulations in Section~\ref{sec:h_results}, and I discuss their implications in Section~\ref{sec:h_discussion}.

\section{Non-equilibrium molecular dynamics}
\label{sec:h_nemd}

The non-equilibrium molecular dynamics (NEMD) method is analogous in format to a steady-state experimental measurement of thermal conductance. Given some sample structure, the simulation adds a steady heat current to one region and removes the same heat current from another region. In response to the heat current between the source and sink, the temperature distribution in the material becomes non-uniform, eventually reaching a steady state. The relationship between the heat current and resulting temperature distribution provides thermal transport properties such as thermal conductivity and thermal conductance.

%For completeness, make some comments here on the literature comparing NEMD with EMD for calculating conductance. I think the general conclusion should be that, if applied in a transparent manner, people have figured out how to understand the differences in the results, so in that sense it's a matter of choice. NEMD also benefits both from being more conceptually straightforward and from having a longer history, and therefore its strengths and weaknesses are better understood.

\subsection{Methodology}
\label{sec:h_nemd_methodology}

All of the NEMD simulations in this work use a similar system geometry. The domain is periodic in all directions; i.e.,\ atomic positions satisfy $r_{i,\alpha} = r_{i,\alpha} \pm L_\alpha$, where $L_\alpha$ is the length of the simulation domain in the $\alpha$ direction.

The three-dimensional domain has dimensions of $6 \times 6 \times 80$ conventional unit cells, as sketched in Figure~\ref{fig:h_nemd_methodology_domain}. %English~et~al.~\cite{English2012} determined that this geometry is sufficiently large to avoid size effects. Size effects are re-examined, however, in Sections~\ref{sec:h_nemd_validation} and~\ref{sec:n_wavelets}.
For a given system temperature, the length of each cubic cell is specified by Equation~\ref{eqn:methods_thiswork_aT} and Table~\ref{tab:methods_thiswork_aT}. The heat current is oriented along the long dimension of the domain ($z$ axis). On each end, two (002) planes are held fixed as walls (144 atoms), and the velocities of the next four (002) planes are scaled at each timestep to add or remove the steady heat current. The length of each ``bulk material''---i.e.,\ the distance from a heat source or sink to the interface---is therefore approximately 20~nm.

The one-dimensional domain has dimensions such that the system has the same total number of atoms, $N = 11520$, and the same number of atoms per region. The length of each bulk material is approximately 1.5~$\upmu$m. The much longer domain was chosen for two reasons: to allow the same degree of statistical averaging as in the 3D system, and to adjust for the smaller thermal resistance of the 1D system.

\begin{figure}[tbp]
  \begin{center}
    \includegraphics{img/h/domain/domain}
    \caption{Geometry of the domain for non-equilibrium simulations in three dimensions.}
    \label{fig:h_nemd_methodology_domain}
  \end{center}
\end{figure}

Each simulation consists of three stages: (1) equilibration at the overall system temperature $T$ with no heat current, (2) ``turning on'' the heat current until the temperature distribution reaches a steady state, and (3) data collection.

\begin{enumerate}
  \item The simulation begins with the atoms in their equilibrium positions, $E^\text{P} = 0$, and with kinetic energy equivalent to twice the nominal temperature. For simplicity, the initial atomic velocities are set to the corresponding uniform magnitude of $|\mathbf{v}| = (2 d k_\text{B} T_\text{nominal} / m)^{1/2}$ with random orientation. Any resulting net momentum of the system is subtracted out from the initial condition to prevent drift. The simulation then runs for 20~ps, at which point the system has reached thermal equilibrium at a true temperature slightly less than $T_\text{nominal}$ due to anharmonicity of the potential energy. Although inelegant, this procedure ensures that simulations at the same $T_\text{nominal}$ have the same true temperature, which can be difficult to enforce precisely if using a thermostat for initialization and switching to NVE for data collection.

  \item After equilibration, the heat source and heat sink are ``turned on.'' Different methods are available for transferring the heat current. They can be grouped into two types: (1) those that maintain a known temperature in the region and (2) those that maintain a known heat current. Whichever quantity is specified, the other is unknown and must be measured during data collection. Both types of methods have been used widely in the literature to calculate thermal transport properties. %: for examples using constant temperature, see Refs.~\cite{Lumpkin1978,Ge1989,Maiti1997,Twu2003,Chen2004,Salaway2008,Ju2010a,Merabia2012,Saaskilahti2014a,Wu2014}; for constant flux, see Refs.~\cite{Lukes2000,Schelling2004,Stevens2007,Aubry2008,Hu2009,Landry2009a,Wang2010d,Duda2011b,Luo2011a,Duda2012c,Duda2012b,Jones2013,Zhou2013,Zhou2013a,Liang2014a,Liang2014}.
    Unfortunately, I am aware of no algorithm that produces an ideal temperature reservoir, which would absorb all incident phonons without reflection while emitting a perfectly thermalized spectrum---i.e.,\ a phononic black body. The development of such algorithms is a challenge in itself and remains an active field of research; see, for example, Ref.~\cite{Ness2015} published this year. In the absence of a better algorithm, I fix the heat current using the scheme described by Jund and Jullien~\cite{Jund1999} for all of the NEMD simulations in this work. In Section~\ref{sec:h_nemd_validation} I assess the effect of this algorithm on the phonon spectra in my simulations.

    In selecting the magnitude of the heat current, it must be large enough to create a measurable $\Delta T$ at the interface, but small enough to remain in the linear regime in which $\dot{Q} \propto \Delta T$ is valid. In addition, the velocity adjustment should correspond to a small ratio of the kinetic energy per atom per timestep. With these constraints in mind, the heat current in each simulation was prescribed proportionally to $T_\text{nominal}$, such that $\dot{Q} = 3.2 \times 10^{-12}$~W for each K in the 1D system or $3.2 \times 10^{-11}$~W for each K in the 3D system. The heat current is applied for 4~ns before data collection to allow the temperature distribution to reach a steady state.

  \item Finally, temperature is sampled locally over time to obtain an accurate measurement of the steady-state temperature distribution. I decompose the system into spatial subdomains of $M$ atoms each and calculate the local temperature of each subdomain as
  \begin{equation}
    \label{eqn:h_nemd_methodology_T}
    T = \frac{1}{d \, k_\text{B}} \frac{1}{M} \sum_i^M m_i \langle v_i^2 \rangle.
  \end{equation}
  Just as with Equation~\ref{eqn:methods_atoms_T}, the use of Equation~\ref{eqn:h_nemd_methodology_T} is problematic if the $M$ atoms in the subdomain are not in thermal equilibrium. In Section~\ref{sec:h_nemd_validation} I present evidence that, in all of the NEMD simulations in this work, the $M$ atoms in each subdomain are very near thermal equilibrium, at least in regard to the atomic picture. The $d \times M$ normal modes of the subdomain, however, may not be near thermal equilibrium, as discussed in Section~\ref{sec:n_results}, so the local temperature calculated using Equation~\ref{eqn:h_nemd_methodology_T} must be interpreted carefully.

  In the 3D systems, I define 160 subdomains, each containing one [002] plane ($M = 144$). Since the heat current is directed along the $z$ axis, all of the atoms in each subdomain must have the same average kinetic energy, and averaging their temperatures is justified. In the 1D systems, no atoms share the same average kinetic energy, but I also use $M = 144$ in order to reduce statistical fluctuations. This is equivalent to applying a smoothing procedure to the average kinetic energies, and since I calculate the conductance by fitting linear models to the temperature distributions, the spatial binning does not affect the calculation.

  The temperature data are collected for 8~ns. The temperature is sampled in each subdomain in intervals of 1~ps. Running averages and standard deviations are stored in memory and written to disk every 40~ps, which reduces access time and disk requirements.

\end{enumerate}

Each simulation thus provides a one-dimensional temperature distribution $T(z)$. I use the common procedure for extracting the thermal conductance at the interface: I fit a linear model to the temperature profiles in the two ``bulk-like'' regions and extrapolate them to the interface. I define $\Delta T$ as the difference between the extrapolated values, from which I calculate $h = \dot{Q} \Delta T^{-1}$.

\subsection{Verification}
\label{sec:h_nemd_validation}

First, I present the results of a single NEMD simulation and, before attempting to calculate the conductance, I assess the details of the transport. The steady-state temperature profile $T(z)$ of a one-dimensional system with Lennard-Jones forces is shown in Figure~\ref{fig:h_nemd_validation_T_lj_Tz}. The nominal temperature of the simulation is $T = 10$~K. Each black dot represents the temperature of a single ``subdomain,'' with the shaded area corresponding to one standard deviation in the temperature fluctuations.

\begin{figure}[tbp]
  \begin{center}
    \includegraphics{img/h/validation_T_lj_Tz}
    \caption[The steady-state temperature profile resulting from a one-dimensional NEMD simulation with Lennard-Jones forces.]{The steady-state temperature profile resulting from a one-dimensional NEMD simulation with Lennard-Jones forces at $T = 10$~K.}
    \label{fig:h_nemd_validation_T_lj_Tz}
  \end{center}
\end{figure}

To assess the thermalization of energy in the system, I arbitrarily select two regions on each side of the interface. One region contains 1440 Ar atoms in $z \in (-0.765,-0.383)$~$\upmu$m, and the second region contains 1440 heavy Ar atoms in $z \in (0.383,0.765)$~$\upmu$m. These regions are marked in red and blue, respectively, in Figure~\ref{fig:h_nemd_validation_T_lj_Tz}, and are labeled with their average temperature. In each region, I assess the extent to which energy has equilibrated among the atomic coordinates (Section~\ref{sec:methods_atoms}) and among the normal mode coordinates (Section~\ref{sec:methods_normalmodes}).

In the atomic coordinates, I first calculate the temperature in each region assuming equipartition, finding $T_\text{atomic,left} \approx 11.3$~K and $T_\text{atomic,right} \approx 9.3$~K. I then compare the Maxwell--Boltzmann distributions at those temperatures,

\begin{equation}
  g_\text{M--B}(v) = \sqrt{\frac{m}{2 \pi k_\text{B} T_\text{atomic}}} \exp \left( - \frac{m v^2}{2 k_\text{B} T_\text{atomic}} \right),
\end{equation}
to the actual distributions of atomic velocities sampled in each region. Those theoretical and measured distributions are plotted in Fig.~\ref{fig:h_nemd_validation_T_lj}a. The agreement suggests that, in each region, energy has indeed equilibrated among the atomic coordinates.

\begin{figure}[tbp]
  \begin{center}
    \includegraphics{img/h/validation_T_lj_speeds}
    \includegraphics{img/h/validation_T_lj_DOS}
    \caption[Atomic speed distributions and densities of states during NEMD simulations in the 1D LJ system.]{Assessing the equilibration of energy during NEMD simulations in the 1D LJ system. (a) Atomic speed distributions sampled during the simulation (dots) and predicted by theory (lines). (b) The density of states sampled from the simulation (colored lines) and predicted (black lines).}
    \label{fig:h_nemd_validation_T_lj}
  \end{center}
\end{figure}

\begin{figure}[tbp]
  \begin{center}
    \includegraphics{img/h/validation_T_harmonic_speeds}
    \includegraphics{img/h/validation_T_harmonic_DOS}
    \caption{The same as Figure~\ref{fig:h_nemd_validation_T_lj}, but in a system with harmonic forces.}
    \label{fig:h_nemd_validation_T_harmonic}
  \end{center}
\end{figure}

I also assess the equilibration of energy among the normal mode coordinates by calculating the density of states in frequency space $D(\omega)$. In each region, the average kinetic energy per frequency per length, $E^\text{K}(\omega)$, is equivalent to the Fourier transform of the temporal autocorrelation of the atomic velocities:

\begin{equation}
  \label{eqn:h_nemd_validation_EK}
  E^\text{K}(\omega) = \mathcal{F} \left\{ \sum_i \frac{m_i}{2} \langle \dot{r}_i(t) \cdot \dot{r}_i(0) \rangle \right\}.
\end{equation}
This kinetic energy spectrum is equal to the energy per mode times the density of states: $E^\text{K}(\omega) = E^\text{K}_{\mathbf{q},\nu}(\omega_{\mathbf{q},\nu}) \, D(\omega)$. The system is classical, so if it is in thermal equilibrium, then the energy per mode is simply $k_\text{B} T$. However, it is not known beforehand how close the system actually is to equilibrium. In Fig.~\ref{fig:h_nemd_validation_T_lj}b, I plot the quantity

\begin{equation}
  \label{eqn:h_nemd_validation_D}
  D_\text{equiv} = \frac{E^\text{K}(\omega)}{k_\text{B} T_\text{equiv}},
\end{equation}
where $D_\text{equiv}$ is only equal to the true density of states, $D(\omega)$, if the system is in thermal equilibrium. I define the equivalent temperature, $T_\text{equiv}$, as the temperature if the system corresponding to the total kinetic energy in the system:\footnote{I use Parseval's theorem along with the fact that, even if the precise form of $D(\omega)$ is unknown, its integral must result in the number of states per length, $2/a$, since in 1D, I set $a$ to twice the interatomic spacing.}

\begin{equation}
  T_\text{equiv} = \frac{a}{2 k_\text{B}} \int E^\text{K}(\omega) d\omega,
\end{equation}
where $E^\text{K}(\omega)$ is measured from the simulation using Equation~\ref{eqn:h_nemd_validation_EK}. This is consistent with the concept used, for example, by Chen~\cite{Chen1998} and by Minnich~\cite{Minnich2015}. I use this equivalent temperature in Eq.~\ref{eqn:h_nemd_validation_D} to plot $D_\text{equiv}(\omega)$ in red and blue in Fig.~\ref{fig:h_nemd_validation_T_lj}b. Also plotted in black is the theoretical density of vibrational states in a one-dimensional chain with harmonic forces:

\begin{equation}
  D_\text{harmonic}(\omega) = \frac{2}{\pi a} \sqrt{\frac{4 k_2}{m} - \omega^2}.
\end{equation}
The sampled $D_\text{equiv}(\omega)$ in each region closely resembles the theoretical $D_\text{harmonic}$ except at high frequencies, which I attribute to broadening due to phonon--phonon scattering in the anharmonic system. Note that the cutoff frequencies are correctly reproduced in comparison with the $\langle 100 \rangle$ dispersion relation shown in Figure~\ref{fig:methods_thiswork_dispersion}. In addition, the calculated values of $T_\text{equiv}$ agree within a few percent with the calculated $T_\text{atomic}$. Overall, I conclude that the energy in the system is well thermalized among both the atomic and normal mode coordinates in the Lennard-Jones system.

For comparison, in Figure~\ref{fig:h_nemd_validation_T_harmonic} I have also plotted the same quantities from an NEMD simulation in a harmonic system.  Due to the lack of anharmonic phonon--phonon scattering as a mechanism for thermalization, one might suspect that energy would not equilibrate among the coordinates. However, the thermal energy does appear well equilibrated. Note that the lack of broadening from phonon--phonon scattering allows the sampled $D_\text{equiv}(\omega)$ to reproduce the ideal $D_\text{harmonic}(\omega)$ very closely. Therefore, I conclude that the energy in the system is well thermalized in the harmonic system as well.\footnote{Closer inspection of Fig.~\ref{fig:h_nemd_validation_T_harmonic}b reveals a non-equilibrium in $D_\text{equiv}(\omega)$ in the Ar. At frequencies below roughly 1.2~THz, $D(\omega)$ exhibits a deficit, which balances an apparent excess in $D(\omega)$ above 1.2~THz. This effect is investigated further in Chapter~\ref{sec:n}, and it does not change the present conclusions.}

At this point, I have shown that the temperature profiles calculated using $T_\text{atomic}$ also correspond to sensible values of $T_\text{equiv}$. The linear fits in Regions 1 and 2 can therefore be used to calculate the conductance $h$ as described above. As a final point of verification, I test whether the magnitudes of heat current used in these simulations are small enough so that the linearity of Equation~\ref{eqn:methods_interfaces_h} is satisfied; i.e.,\ so that $h$ is constant with $\dot{Q}$. Figure~\ref{fig:h_nemd_validation_Q} shows the conductances measured in both one- and three-dimensional systems as a function of the imposed heat current. The error bars denote $\pm 2 s / \sqrt{n}$, where $s$ is the standard deviation of $n=6$ simulations with randomized initial conditions. The heat currents used in the final simulations are marked with arrows. Based on these results, I am satisfied that the heat currents are large enough to resolve $h$ but small enough to remain linear in $\Delta T$.

\begin{figure}[tbp]
  \begin{center}
    \includegraphics{img/h/validation_Q_1d}
    \includegraphics{img/h/validation_Q_3d}
    \caption[The thermal conductance $h$ measured using a range of heat currents $\dot{Q}$ in 1D and 3D.]{The thermal conductance $h$ measured using a range of heat currents $\dot{Q}$ in one-dimensional systems (left) and three-dimensional systems (right).}
    \label{fig:h_nemd_validation_Q}
  \end{center}
\end{figure}

%Show that I get the same results as some other Ar/h-Ar system. Candidates are Stevens2007~\cite{Stevens2007}, Ju2010a~\cite{Ju2010a}, Jones2013~\cite{Jones2013}. In addition, show that these conductances are in the ballpark of experimental values.

\section{Results: Thermal conductance}
\label{sec:h_results}

To test the theories for $h(T)$ described in Section~\ref{sec:methods_interfaces}, I use this simulation scheme to calculate $h(T)$ at Ar/heavy Ar interfaces. In both one- and three-dimensional systems, I report the conductance with three different arrangements of interatomic forces:
\begin{enumerate}[label={\alph*)}]
  \item all Lennard-Jones (anharmonic),
  \item all harmonic, and
  \item all harmonic except between the two atoms (1D) or atomic planes (3D) immediately adjacent to the interface, which interact through the Lennard-Jones potential.
\end{enumerate}
In presenting the following results, I use the letters (a), (b), and (c) to refer to these arrangements of forces.

\subsection{In one dimension}
\label{sec:h_results_1d}

Three representative steady-state temperature profiles collected in one-dimensional systems are shown in Figure~\ref{fig:h_results_Tz_1d}. Each corresponds to a different arrangement of forces as described above. The resulting conductance $h$ is inversely proportional to the temperature drop at the interface, and since the same heat current is used in all three simulations at $T_\text{nominal} = 6~K$, the relative magnitudes of $\Delta T$ can be directly compared across the three plots. The calculated conductance is therefore highest in (a), and it is smaller in (b) and (c) in which the values $\Delta T$ are roughly equal. Aside from the conductance, $T(z)$ in the bulk materials has a significantly greater slope in (a) than in (b) and (c), corresponding to a smaller thermal conductivity. This is consistent with the anharmonic forces in (a) compared to the harmonic forces in the bulk materials in (b) and (c), which exhibit practically horizontal profiles (i.e.,\ $k\ \to \infty$). 

\begin{figure}[tbp]
  \begin{center}
    \includegraphics{img/h/Tz_lj_1d}
    \includegraphics{img/h/Tz_harmonic_1d}
    \includegraphics{img/h/Tz_lj_0uc_1d}
    \caption[Steady-state temperature profiles in one-dimensional systems with (a) all LJ bonds, (b) all harmonic bonds, and (c) all harmonic bonds except a single LJ bond at the interface.]{Steady-state temperature profiles in one-dimensional systems at $T_\text{nominal} = 6$~K with (a) all LJ bonds, (b) all harmonic bonds, and (c) all harmonic bonds except a single LJ bond at the interface.}
    \label{fig:h_results_Tz_1d}

    \vspace{0.2in}

    \includegraphics{img/h/h_1d}
    \caption{The thermal conductance $h(T)$ between one-dimensional Ar and heavy Ar.}
    \label{fig:h_results_1d}
  \end{center}
\end{figure}

These simulations are repeated across a range of nominal temperatures from $T = 2$ to 12~K, in increments of 2~K, to collect the data displayed in Figure~\ref{fig:h_results_1d}. Each data point is the mean of $n=30$ simulations,\footnote{This is a much larger number of simulations than typical, which was necessitated by the much wider temperature fluctuations in the 1D profiles than in 3D: compare Figures~\ref{fig:h_results_Tz_1d} and the error bars denote $\pm 2 s/\sqrt{n}$. and~\ref{fig:h_results_Tz_3d}.} The relative magnitudes of $h$ resulting from different arrangements of forces are consistent with the comparison among the single simulations. Furthermore, note that the ``excess'' conductance in the LJ system grows with temperature, while the conductance in the harmonic system is nearly perfectly constant with $T$. The conductance in the harmonic system with the LJ interface is slightly higher, which is not readily apparent in Figure~\ref{fig:h_results_Tz_1d}, but by a much smaller margin than in the all-LJ system. For comparison, I've also plotted the value of interfacial conductance predicted using Eq.~\ref{eqn:methods_interfaces_h} under the assumptions that $n_{\mathbf{q},\nu} \to n_\text{B--E}(\omega_{\mathbf{q},\nu},T)$ and that scattering is elastic. In a one-dimesional, classical system, this gives~\cite{Lumpkin1978}

\begin{equation}
  \label{eqn:h_results_1d_theory}
  h = \frac{k_\text{B}}{2 \pi} \int_0^\infty \alpha(\omega) \, d \omega,
\end{equation}
where the transmissivity as a function of phonon frequency, $\alpha(\omega)$, is given by Eq.~\ref{eqn:alpha_packets_validation_salt}. Despite the crude assumptions, the model agrees well with the conductance values calculated in the harmonic system, both in magnitude and in the lack of temperature dependence.

\subsection{In three dimensions}
\label{sec:h_results_3d}

Likewise, three temperature profiles collected in three-dimensional systems at $T_\text{nominal} = 30$~K are shown in Figure~\ref{fig:h_results_Tz_3d}. They are qualitatively similar to the one-dimensional temperature profiles, except that the conductance in case (c) is significantly larger than case (b), although still not nearly as large as in case (a).

\begin{figure}[tbp]
  \begin{center}
    \includegraphics{img/h/Tz_lj_3d}
    \includegraphics{img/h/Tz_harmonic_3d}
    \includegraphics{img/h/Tz_lj_0uc_3d}
    \caption[Steady-state temperature profiles in three-dimensional systems with (a) all LJ bonds, (b) all harmonic bonds, and (c) all harmonic bonds except a single LJ bond at the interface.]{Steady-state temperature profiles in three-dimensional systems at $T_\text{nominal} = 30$~K with (a) all LJ bonds, (b) all harmonic bonds, and (c) all harmonic bonds except a single LJ bond at the interface.}
    \label{fig:h_results_Tz_3d}

    \vspace{0.2in}

    \includegraphics{img/h/h_3d}
    \caption{The thermal conductance $h(T)$ in three-dimensional Ar and heavy Ar.}
    \label{fig:h_results_3d}
  \end{center}
\end{figure}

These simulations are repeated across a range of nominal temperatures from $T = 2$ to 50~K, in increments of 4~K, to collect the data displayed in Figure~\ref{fig:h_results_3d}. Each data point is the mean of $n=10$ simulations, and the error bars denote $\pm 2 s/\sqrt{n}$. The qualitative results are similar to those of the one-dimensional simulations, although $h$ appears to take a different functional dependence on $T$.

\section{Discussion}
\label{sec:h_discussion}

The results, both in the one- and three-dimensional systems, are consistent with hypotheses that elevated conductance at high temperatures arises due to some type of inelastic phonon processes, as enabled by anharmonic forces. However, they indicate that accounting for inelastic transmission in the phonon transmissivity (Section~\ref{sec:methods_interfaces_alpha}) is unlikely to explain $h(T)$.

Rather, they suggest that accounting for inelastic effects on the phonon distribution (Section~\ref{sec:methods_interfaces_n}) is likely more important. However, these results do not provide enough information to distinguish between the two types of models presented in that section: (1) the heat flux effect and (2) the interfacial boundary condition effect. These conclusions are based on the following observations:

\begin{itemize}
  \item Having anharmonicity throughout the system raises the conductance significantly in both one- and three-dimensional systems. Having anharmonicity at only the interface also raises conductance, but only slightly. This lends support to models incorporating inelastic processes not only at the interface itself, but in some nearby region as well.
  \item Unfortunately, as mentioned in Section~\ref{sec:h_nemd}, the algorithm used to enforce the heat current does not produce a known energy spectrum. In addition, the energy spectrum calculated using Equation~\ref{eqn:h_nemd_validation_EK} cannot distinguish between left-traveling and right-traveling modes, making it difficult to assess the models for the flux-induced non-equilibrium distribution proposed by Simons~\cite{Simons1974} and others.
  \item Figure~\ref{fig:h_nemd_validation_T_harmonic} also suggests that an interface-induced non-equilibrium distribution persists into the bulk argon material. This is due to the ballistic transport in the harmonic system, consistent with the slight non-equilibrium energy distribution suggested in Figure~\ref{fig:h_nemd_validation_T_harmonic}.
\end{itemize}
In Chapter~\ref{sec:alpha}, I present results to assess the first point in more detail, while in Chapter~\ref{sec:n} I present results that help to further distinguish between the second and third.
